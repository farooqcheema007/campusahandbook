package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import com.tbox.imagedownload.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Deal_Category extends Activity_Base implements  OnClickListener {
	ProgressBar							pb_deal_category;
	ArrayList<Deals> 					dealList;
	static ArrayList<JSONObject> 		json_array_list;
	String 								category_id,category_type;		
	ListView 							lv;
	ImageView							iv_deals_category_type;
	TextView							tv_main_heading;
	int 								value=0;
	
	private void Registerwidgets(){
		dealList 			= 	new ArrayList<Deals>();
		pb_deal_category 	= 	(ProgressBar)findViewById(R.id.progressBarDealCategory);
		json_array_list		=	new ArrayList<JSONObject>();
		lv					=	(ListView) findViewById(R.id.listViewDealsCategory);
		iv_deals_category_type	=	(ImageView)findViewById(R.id.mainSubHeadingDeals);
		tv_main_heading=(TextView)findViewById(R.id.mainHeadingTextDeals);
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Big Deals");
		super.setBaseLayout(R.layout.activity_deal_category);
		Registerwidgets();
		Bundle bundle=getIntent().getExtras();
		category_id = bundle.getString("coupen_id");
		category_type = bundle.getString("coupen_type");
		mainSubHeadingType(bundle.getString("coupen_name"));
		screenResolution();
		sendJsonRequest();
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
//		Registerwidgets();
//		sendJsonRequest();
		super.onResume();
	}


	private void sendJsonRequest(){
		sendJsonRequestLoopi();
//		ListView lv=(ListView) findViewById(android.R.id.list);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent 	=	new Intent(Activity_Deal_Category.this,Activity_Deal_Category_Details.class);
				Bundle bundle	=	new Bundle();
				bundle.putString("jsonObject", json_array_list.get(arg2).toString());
				bundle.putInt("status", 1);
				bundle.putInt("value", 1);
				intent.putExtras(bundle);
				startActivity(intent);	
			}
		});

		
	}
	private void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("category_id",category_id);
		params.put("type",category_type);
		params.put("api_key",settings.getString("key", null).toString());
		
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_deals_data, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
						 	JSONArray jArray1=object.getJSONArray("coupons");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							Deals dealObj = new Deals();
	 							dealObj.coupon_title = jobject1.getString("coupon_title");
	 							dealObj.coupen_place = jobject1.getString("place_at");
	 							dealObj.coupen_high_five = jobject1.getString("high_five");
	 							dealObj.coupen_image = jobject1.getString("coupon_image");
	 				            json_array_list.add(jobject1);  	
	 				            dealList.add(dealObj);
						    }
		 					pb_deal_category.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv.setAdapter(listAdapter);
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_deal_category.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Deal_Category.this);
					}
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	class ListAdapter1 extends ArrayAdapter<Deals>{

	 	public ListAdapter1() {
	 		super(Activity_Deal_Category.this, R.layout.inflate_big_deal_catagory,dealList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_big_deal_catagory, null);
 		    }
			ImageView iv_main = (ImageView)convertView.findViewById(R.id.imageviewBigDealCategory);
 			TextView tv_title = (TextView)convertView.findViewById(R.id.tVBigDealCategoryHeading);
 			TextView tv_sub_title = (TextView)convertView.findViewById(R.id.tVBigDealCategorySubHeading);
 			TextView tv_high_five = (TextView)convertView.findViewById(R.id.tVDealCategoryHighFiveText);

 			tv_title.setText(dealList.get(position).coupon_title);
 			tv_sub_title.setText(dealList.get(position).coupen_place);
 			tv_high_five.setText(dealList.get(position).coupen_high_five);
 			ImageLoader imageLoader	=	new ImageLoader(getApplicationContext());
 			
 			if(value==1080)
 				imageLoader.DisplayImage(dealList.get(position).coupen_image, iv_main);
 			else if(value==480)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_hdpi_480"), iv_main);
 			else if(value==720)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_xhdpi"), iv_main);
 			else if(value==600)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_tab7"), iv_main);
 			else if(value==800)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_tab10"), iv_main);
 			return convertView;	
		}
	 	

	}
	
	private void mainSubHeadingType(String strName){
		tv_main_heading.setText(strName);
	}
	private void screenResolution(){
		value = Integer.parseInt(getResources().getString(R.string.whichscreen));
	}
	private String imagePath(String imagePath , String addPath){
//		int strLength = imagePath.length();
//		String strName = imagePath.split("\");
		String first = imagePath.substring(0, imagePath.length() -4);
		String second = imagePath.substring(imagePath.length()-4);
//		String[] str = imagePath.split("\\.");
		
		return first+addPath+second;
	}

}

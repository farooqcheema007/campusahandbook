package com.tbox.campus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campus.R;

public class Activity_Calender extends Activity_Base {
	
	public GregorianCalendar 	month, itemmonth;// calendar instances.
//	LinearLayout rLayout;
	public BaseAdapterCalender 		adapter;// adapter instance
	public Handler 				handler;// for grabbing some event values for showing the dot
	public ArrayList<String>	items; // container to store calendar items which
	GridView gridview;
	ArrayList<String> event,date,desc;
	static String monthName;
	static int index;
	TextView title;

	private void Registerwidgets(){
//		rLayout = (LinearLayout) findViewById(R.id.text);
		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		adapter = new BaseAdapterCalender(this, month);
		gridview = (GridView) findViewById(R.id.gridview);
		handler = new Handler();
		items = new ArrayList<String>();
		date = new ArrayList<String>();
		desc = new ArrayList<String>();
	}
	
	private void createCalenderInterface(){
		gridview.setAdapter(adapter);
		handler.post(calendarUpdater);
		
		title = (TextView) findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
		BaseAdapterCalender.strName = title.getText().toString();
				
		TextView previous = (TextView) findViewById(R.id.previoustmonth);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
			}
		});

		TextView next = (TextView) findViewById(R.id.nextmonth);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();

			}
		});
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// removing the previous view if added
				index = position;
				monthName = title.getText().toString();
				gridview.invalidateViews();
				Intent i = Activity_Calender.this.getPackageManager().getLaunchIntentForPackage("com.android.calendar");
				if (i != null)
				  startActivity(i);
			}

		});
		
		
	}
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Calender");
		super.setBaseLayout(R.layout.activity_calender);	
		Locale.setDefault(Locale.US);
		Registerwidgets();
		index = 100;
		createCalenderInterface();
	}
	@Override
	public void onResume() {
	    super.onResume();
	}

	@Override
	public void onPause() {

	    super.onPause();
	}
	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}
	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}
	}

	protected void showToast(String string) {
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();

	}

	public void refreshCalendar() {
		title = (TextView) findViewById(R.id.title);

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		handler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
		BaseAdapterCalender.strName = title.getText().toString();
	}
	
	public Runnable calendarUpdater = new Runnable() {

		@Override
		public void run() {
			items.clear();

			// Print dates of the current week
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			String itemvalue;
			event = Utils.readCalendarEvent(Activity_Calender.this);
			Log.d("=====Event====", event.toString());
			Log.d("=====Date ARRAY====", Utils.startDates.toString());

			for (int i = 0; i < Utils.startDates.size(); i++) {
				itemvalue = df.format(itemmonth.getTime());
				itemmonth.add(GregorianCalendar.DATE, 1);
				items.add(Utils.startDates.get(i).toString());
			}
			adapter.setItems(items);
			adapter.notifyDataSetChanged();
		}
	};
}

package com.tbox.campus;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Deals_Of_Week extends Activity_Base implements OnClickListener{
	
	Button				btn_view_no_thanx ,btn_view_detail; 
	ImageView 			ivDeals;
	ImageLoader 		imageLoader;
	JSONObject			json_array_list;
	String				coupens_image;
	ProgressBar			pb;
	TextView			tvDealOff,tvDealName;
	String 				offPercentageValue,offName;
	RelativeLayout		lLayoutDeal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Deal of the Week");
		super.setBaseLayout(R.layout.activity_deal_week);
		Registerwidgets();
		getBannerImagesAndData();
	}
	private void Registerwidgets() {
		imageLoader = new ImageLoader(getApplicationContext());
		tvDealOff = (TextView)findViewById(R.id.dealOffWeakPercentage);
		btn_view_detail = (Button)findViewById(R.id.dealDayView);
		btn_view_no_thanx = (Button)findViewById(R.id.dealDayThanx);
//		json_array_list	= new ArrayList<JSONObject>();
//		coupens_image= new ArrayList<String>();
		pb = (ProgressBar)findViewById(R.id.progressBarDealDay);
		ivDeals = (ImageView)findViewById(R.id.ivDealOfWeek);
		lLayoutDeal=(RelativeLayout)findViewById(R.id.linearLayoutDealOfWeek);
		tvDealName = (TextView)findViewById(R.id.dealOffWeakName);
		btn_view_no_thanx.setOnClickListener(this);
	}
	private void setListner(){
		btn_view_detail.setOnClickListener(this);
//		btn_view_no_thanx.setOnClickListener(this);
	}

	private void setValues(){
		imageLoader.DisplayImage(coupens_image, ivDeals);
		tvDealName.setText(offName);
		tvDealName.setVisibility(View.VISIBLE);
		tvDealOff.setText(offPercentageValue);
		tvDealOff.setVisibility(View.VISIBLE);
		ivDeals.setVisibility(View.VISIBLE);
		pb.setVisibility(View.INVISIBLE);
		
	}
	//Response for Add News
	private void getBannerImagesAndData(){
			RequestParams params = new RequestParams();
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
			String id=""+settings.getInt("schoolid", 0);
			params.put("school_id",id.toString());
			params.put("api_key",settings.getString("key", null).toString());
	        
	        //send get request for New Details
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.get(Constant.addess_url_base+Constant.address_deal_of_week, params , new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
	            	int status = 0;
					try {
						status = object.getInt("status");
						 if(status ==1){ 
//		 						JSONArray jArray1=object.getJSONArray("coupon");
//		 						for(int i=0;i<jArray1.length();i++){
		 							JSONObject	jobject1 = 	object.getJSONObject("coupon");
		 							coupens_image 		= 	jobject1.getString("coupon_image");
		 							offName				= 	jobject1.getString("place_at");
		 							offPercentageValue 	= 	jobject1.getString("coupon_title");
		 							json_array_list		=	jobject1;
//		 						}
		 							setValues();
		 							setListner();
						 }
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
	            	
	             }
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
	            	pb.setVisibility(View.INVISIBLE);
	            	btn_view_no_thanx.setVisibility(View.VISIBLE);
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0)
							Utils.showAlertDialog(jobject.getString("error_message"), Activity_Deals_Of_Week.this);
						
					} catch (Exception ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}

	             }
	    	});

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.dealDayView:
			Intent intent_detaisl 	=	new Intent(Activity_Deals_Of_Week.this,Activity_Deal_Category_Details.class);
			Bundle bundle	=	new Bundle();
			bundle.putString("jsonObject",json_array_list.toString());
			bundle.putInt("status", 1);
			bundle.putInt("value", 0);
			intent_detaisl.putExtras(bundle) ;
			startActivity(intent_detaisl);	
			break;
		case R.id.dealDayThanx:
			Intent intent_home 	=	new Intent(Activity_Deals_Of_Week.this,Activity_Main_Fragment.class);
			intent_home.putExtra("key", 0);
			startActivity(intent_home);	
			finish();
			break;
		default:
			break;
		}
	}


}

package com.tbox.campus;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.example.campus.R;

public class Activity_Base extends Activity implements OnClickListener{
	public View childHeader, childFooter,childHeadingBar,childView;
	ViewGroup viewGroup;
	RelativeLayout rl = null;
	
//	private TextView tv_home,tv_profile;
	private LinearLayout slidingPanel;
	private boolean isExpanded;
	private DisplayMetrics metrics;
//	private RelativeLayout headerPanel;
	private RelativeLayout menuPanel;
	private int panelWidth;
//	private ImageView menuViewButton;
	EditText search;
	
	
	FrameLayout.LayoutParams menuPanelParameters;
	FrameLayout.LayoutParams slidingPanelParameters;
	LinearLayout.LayoutParams headerPanelParameters ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.base_activity);
		rl = (RelativeLayout)findViewById(R.id.baseLayout); 
		setFooter();
		
//		tv_home=(TextView)findViewById(R.id.home);
//		tv_profile=(TextView)findViewById(R.id.profile);
//		tv_home.setOnClickListener(this);
		
//		tv_home.setOnClickListener(new View.OnClickListener() {
//
//			  @Override
//			  public void onClick(View v) {
//				  isExpanded = false;
//				  enableDisableViewGroup(viewGroup,true);
//		    		new CollapseAnimation(slidingPanel,panelWidth,
//			        	    TranslateAnimation.RELATIVE_TO_SELF, 0.75f,
//			        	    TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0, 0.0f);
//			  }
//
//		});
//		tv_profile.setOnClickListener(new View.OnClickListener() {
//
//			  @Override
//			  public void onClick(View v) {
//				  isExpanded = false;
//				  	Intent intent_profile =new Intent(Activity_Base.this,Activity_Profile.class);
//			    	startActivityForResult(intent_profile,4001);
//			    	Activity_Base.this.overridePendingTransition(R.anim.left_anim,R.anim.right_in);
//				  
////		    		new CollapseAnimation(slidingPanel,panelWidth,
////			        	    TranslateAnimation.RELATIVE_TO_SELF, 0.75f,
////			        	    TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0, 0.0f);
////		    		Activity_Home homeObj = new Activity_Home();
////		    		homeObj.setBackgroundLayout(Activity_Base.this);
//		    		
//			  }
//
//		});

		
		
		search=(EditText)findViewById(R.id.search);
		search.setFocusable(false);
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		panelWidth = (int) ((metrics.widthPixels)*0.75);
			
		menuPanel = (RelativeLayout) findViewById(R.id.menuPanel);
		menuPanelParameters = (FrameLayout.LayoutParams) menuPanel.getLayoutParams();
		menuPanelParameters.width = panelWidth;
		menuPanel.setLayoutParams(menuPanelParameters);
		
		slidingPanel = (LinearLayout) findViewById(R.id.slidingPanel);
		slidingPanelParameters = (FrameLayout.LayoutParams) slidingPanel.getLayoutParams();
		slidingPanelParameters.width = metrics.widthPixels;
		slidingPanel.setLayoutParams(slidingPanelParameters);
		
	}
	void setHeader(boolean activityStatusBtn,boolean mainHeadingStatus,String headingText){
		LayoutInflater flater = LayoutInflater.from(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        childHeader = flater.inflate(R.layout.header,rl, false);
        childHeader.setLayoutParams(params);
        childHeader.setId(2000);
		TextView tv_date = (TextView)childHeader.findViewById(R.id.headerDate);
		Calendar c = Calendar.getInstance();
		String strDate = c.getTime().toString();
		
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		String month_name = month_date.format(cal.getTime());
		
		SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
		String goal = outFormat.format(cal.getTime());
		
		String[] arrDate = strDate.split(" ");
		tv_date.setText(goal+"  "+month_name+" "+arrDate[2]+" , "+arrDate[5]);
		tv_date.setTextColor(Color.WHITE);
		Button btn_header = (Button)childHeader.findViewById(R.id.headerBtn);
//		btn_header.setOnClickListener(this);
		if (activityStatusBtn)
			btn_header.setVisibility(View.VISIBLE);
		else 
			btn_header.setVisibility(View.INVISIBLE);
		
		rl.addView(childHeader);
		
		int dpValue = 35; // margin in dips
		float d = this.getResources().getDisplayMetrics().density;
		int margin = (int)(dpValue * d); // margin in pixels
		
		LayoutInflater inflaterLayout = LayoutInflater.from(this);
		RelativeLayout.LayoutParams paramsHeading = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		paramsHeading .addRule(RelativeLayout.BELOW,childHeader.getId());
		paramsHeading .setMargins(0, margin, 0, 0);
        childHeadingBar = inflaterLayout.inflate(R.layout.main_heading, null);
        childHeadingBar.setLayoutParams(paramsHeading);

        childHeadingBar.setId(2003);
		
		ImageView ivHeading = (ImageView)childHeadingBar.findViewById(R.id.mainHeading);
		TextView mainHeadingText = (TextView)childHeadingBar.findViewById(R.id.mainHeadingText);
		if (mainHeadingStatus){
			ivHeading.setVisibility(View.VISIBLE);
			mainHeadingText.setVisibility(View.VISIBLE);
			mainHeadingText.setText(headingText);
		}else{
			ivHeading.setVisibility(View.INVISIBLE);
			mainHeadingText.setVisibility(View.INVISIBLE);		
		}
		
		rl.addView(childHeadingBar);
		
		btn_header.setOnClickListener(new View.OnClickListener() {
	         @Override
	          public void onClick(View v) {
	 			search.setFocusable(true);
		    	if(!isExpanded){
		    		enableDisableViewGroup(viewGroup,false);
		    		isExpanded = true;   		    				        		
		    		search.setFocusable(false);
		    		//Expand
		    		new ExpandAnimation(slidingPanel, panelWidth,
		    	    Animation.RELATIVE_TO_SELF, 0.0f,
		    	    Animation.RELATIVE_TO_SELF, 0.75f, 0, 0.0f, 0, 0.0f);		    			         	    
		    	}else{
		    		isExpanded = false;
		    		
		    		//Collapse
		    		enableDisableViewGroup(viewGroup,true);
		    		new CollapseAnimation(slidingPanel,panelWidth,
	        	    TranslateAnimation.RELATIVE_TO_SELF, 0.75f,
	        	    TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f, 0, 0.0f);
		    	}
//	             toMenuOnClick(v);
	           }
	         });
	}
	void setFooter(){
		
		LayoutInflater flater = LayoutInflater.from(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        childFooter = flater.inflate(R.layout.footer, null);
        childFooter.setLayoutParams(params);
        childFooter.setId(2001);
        rl.addView(childFooter);

	}
	
	public void setBaseLayout(int id) {
		LayoutInflater flater = LayoutInflater.from(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW,childHeader.getId());
        params.addRule(RelativeLayout.ABOVE,childFooter.getId());
        childView = flater.inflate(id, null);
        viewGroup = (ViewGroup) childView;
        childView.setLayoutParams(params);
        rl.addView(childView);
        childHeadingBar.bringToFront();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		}
	}
	
	 public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
		    int childCount = viewGroup.getChildCount();
		    for (int i = 0; i < childCount; i++) {
		      View view = viewGroup.getChildAt(i);
		      view.setEnabled(enabled);
		      if (view instanceof ViewGroup) {
		        enableDisableViewGroup((ViewGroup) view, enabled);
		      }
		    }
		  }
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        overridePendingTransition(R.anim.left_anim, R.anim.right_in);
	    }
}

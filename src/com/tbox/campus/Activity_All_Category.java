package com.tbox.campus;


import java.util.ArrayList;

import com.tbox.campus.Activity_Deal_Category.ListAdapter1;
import com.tbox.classes.*;
import com.tbox.imagedownload.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_All_Category extends Activity_Base implements OnClickListener{
	ArrayList<Coupens>	arrCoupens = new ArrayList<Coupens>();
	ListView lv;
	ProgressBar pb;
	ArrayList<Deals> dealList; 
	static ArrayList<JSONObject> json_array_list;
	int value=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Big Deals");
		super.setBaseLayout(R.layout.activity_all_category);
		lv = (ListView)findViewById(R.id.listViewAllCategory);
		pb=(ProgressBar)findViewById(R.id.progressBarAllCategory);
		json_array_list	=	new ArrayList<JSONObject>();
		dealList = 	new ArrayList<Deals>();
		screenResolution();
		sendJsonRequest();
	}
	private void sendJsonRequest(){
		sendJsonRequestLoopi();
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				Intent intent 	=	new Intent(Activity_All_Category.this,Activity_Deal_Category_Details.class);
				Bundle bundle	=	new Bundle();
				bundle.putString("jsonObject", json_array_list.get(arg2).toString());
				bundle.putInt("status", 1);
				bundle.putInt("value", 1);
				intent.putExtras(bundle);
				startActivity(intent);	
				
//				Intent intent_deal_bueaty=new Intent(Activity_All_Category.this,Activity_Deal_Category.class);
//				Bundle bundle_bueaty =new Bundle();
//				bundle_bueaty.putString("coupen_id",arrCoupens.get(arg2).coupon_category_id);
//				bundle_bueaty.putString("coupen_type",arrCoupens.get(arg2).type);
//				bundle_bueaty.putString("coupen_name",arrCoupens.get(arg2).coupon_category);
//				intent_deal_bueaty.putExtras(bundle_bueaty) ;
//				startActivity(intent_deal_bueaty);
			}
		});

		
	}
	private void sendJsonRequestLoopi(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
		params.put("type","1");
		params.put("category_id","-1");
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_all_category_deal , params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
						 	JSONArray jArray1=object.getJSONArray("coupons");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							Deals dealObj = new Deals();
	 							dealObj.coupon_title = jobject1.getString("coupon_title");
	 							dealObj.coupen_place = jobject1.getString("place_at");
	 							dealObj.coupen_high_five = jobject1.getString("high_five");
	 							dealObj.coupen_image = jobject1.getString("coupon_image");
	 				            json_array_list.add(jobject1);  	
	 				            dealList.add(dealObj);
						    }
		 					pb.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv.setAdapter(listAdapter);

//	 						JSONArray jArray1=object.getJSONArray("coupons_category");
//	 						for(int i=0;i<jArray1.length();i++){
//	 							Coupens coupensObject = new Coupens();
//	 							JSONObject	jobject1 = jArray1.getJSONObject(i);
//	 							coupensObject.type				=	jobject1.getString("type");
//	 							coupensObject.coupon_category	=	jobject1.getString("coupon_category");
//	 							coupensObject.coupon_category_id=	jobject1.getString("coupon_category_id"); 
//	 							arrCoupens.add(coupensObject);
//	 							Deals dealObj = new Deals();
//	 							dealObj.coupon_title = jobject1.getString("coupon_category");
//	 							dealList.add(dealObj);
//			 					ListAdapter1 listAdapter=new ListAdapter1();
//			 					lv.setAdapter(listAdapter);
//
//	 						}
//	 						pb.setVisibility(View.INVISIBLE);
//	 						setListner();
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	 pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_All_Category.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	class ListAdapter1 extends ArrayAdapter<Deals>{

	 	public ListAdapter1() {
	 		super(Activity_All_Category.this, R.layout.inflate_big_deal_catagory,dealList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_big_deal_catagory, null);
 		    }
			ImageView iv_main = (ImageView)convertView.findViewById(R.id.imageviewBigDealCategory);
 			TextView tv_title = (TextView)convertView.findViewById(R.id.tVBigDealCategoryHeading);
 			TextView tv_sub_title = (TextView)convertView.findViewById(R.id.tVBigDealCategorySubHeading);
 			TextView tv_high_five = (TextView)convertView.findViewById(R.id.tVDealCategoryHighFiveText);

 			tv_title.setText(dealList.get(position).coupon_title);
 			tv_sub_title.setText(dealList.get(position).coupen_place);
 			tv_high_five.setText(dealList.get(position).coupen_high_five);
 			ImageLoader imageLoader	=	new ImageLoader(getApplicationContext());
 			
 			if(value==1080)
 				imageLoader.DisplayImage(dealList.get(position).coupen_image, iv_main);
 			else if(value==480)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_hdpi_480"), iv_main);
 			else if(value==720)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_xhdpi"), iv_main);
 			else if(value==600)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_tab7"), iv_main);
 			else if(value==800)
 				imageLoader.DisplayImage(imagePath(dealList.get(position).coupen_image,"_tab10"), iv_main);
 			return convertView;	
		}
	
		private String imagePath(String imagePath , String addPath){
			String first = imagePath.substring(0, imagePath.length() -4);
			String second = imagePath.substring(imagePath.length()-4);			
			return first+addPath+second;
		}

	 	

	}
	private void screenResolution(){
		value = Integer.parseInt(getResources().getString(R.string.whichscreen));
	}
	
	
}

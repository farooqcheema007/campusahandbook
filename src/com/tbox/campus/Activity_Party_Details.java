package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Party_Details extends Activity_Base{
	String 								category_id,category_name;
	ListView							lv; 
	static ArrayList<JSONObject> 		json_array_list;
	ArrayList<PartyDetails>				partyList;	
	ProgressBar							pb_deal_category;
	
	private void Registerwidgets(){
		partyList 			= 	new ArrayList<PartyDetails>();
		pb_deal_category 	= 	(ProgressBar)findViewById(R.id.progressBarPartyDetail);
		json_array_list		=	new ArrayList<JSONObject>();
		lv					=	(ListView) findViewById(R.id.listViewPartyDetails);
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle=getIntent().getExtras();
		category_id = bundle.getString("category_id");
		category_name=bundle.getString("event_category");
		super.setHeader(false,true,category_name);
		super.setBaseLayout(R.layout.activity_party_details);
		
		Registerwidgets();
		sendJsonRequest();
	}
	void sendJsonRequest(){
		sendJsonRequestLoopi();
//		ListView lv=(ListView) findViewById(android.R.id.list);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent 	=	new Intent(Activity_Party_Details.this,Activity_Party_Details_Read.class);
				Bundle bundle	=	new Bundle();
				bundle.putString("jsonObject", json_array_list.get(arg2).toString());
				bundle.putString("event_catgory", category_name);
				intent.putExtras(bundle) ;
				startActivity(intent);	
			}
		});

		
	}
	void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("category_id",category_id);
		params.put("api_key",settings.getString("key", null).toString());
		
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_party_details, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
						 	JSONArray jArray1=object.getJSONArray("parties");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							PartyDetails partyObj = new PartyDetails();
	 							partyObj.event_id = jobject1.getString("event_id");
	 							partyObj.event_image=jobject1.getString("event_image");
	 							partyObj.event_name=jobject1.getString("event_name");
	 							partyObj.event_deal1 = jobject1.getString("event_deal1");
	 							partyObj.event_deal2 = jobject1.getString("event_deal2");
	 							partyObj.event_high_five = jobject1.getString("high_five");
	 				            json_array_list.add(jobject1);  	
	 				            partyList.add(partyObj);
						    }
		 					pb_deal_category.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv.setAdapter(listAdapter);
						}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_deal_category.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Party_Details.this);
					}
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}

	class ListAdapter1 extends ArrayAdapter<PartyDetails>{

	 	public ListAdapter1() {
	 		super(Activity_Party_Details.this, R.layout.inflate_party_details,partyList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_party_details, null);
 		    }
 			TextView tv_name = (TextView)convertView.findViewById(R.id.partyDetailsHeading);
 			TextView tv_deal1 = (TextView)convertView.findViewById(R.id.partyDetailsPlace);
 			TextView tv_deal2 = (TextView)convertView.findViewById(R.id.partyDetailsMenu);
 			TextView tv_high_five = (TextView)convertView.findViewById(R.id.tvPartyDetailsViewHighFiveText);
 			ImageView iv_image = (ImageView)convertView.findViewById(R.id.ivPartyDetailsView);
 			tv_name.setText(partyList.get(position).event_name);
 			tv_deal1.setText(partyList.get(position).event_deal1);
 			tv_deal2.setText(partyList.get(position).event_deal2);
 			tv_high_five.setText(partyList.get(position).event_high_five);
 			ImageLoader imageLoader	=	new ImageLoader(getApplicationContext());
 			imageLoader.DisplayImage(partyList.get(position).event_image, iv_image);
 			
 			return convertView;	
		}
	 	

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}


}

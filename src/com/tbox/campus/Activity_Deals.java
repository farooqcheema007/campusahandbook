package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Deals extends Activity_Base implements OnClickListener{
	Button		bAuto , bNightLife , bBeauty , bDining , bElectronics ,	bHealth , bHosing , 
					bLegalServices , bMiscellaneous , bReaction ,bAllCategories;
	ProgressBar	pb_deals;
	ArrayList<Coupens>	arrCoupens = new ArrayList<Coupens>();
	
	
	private void Registerwidgets(){
		bAuto 			= 	(Button)findViewById(R.id.dealAuto);
		bNightLife 		= 	(Button)findViewById(R.id.dealNightLife);
		bDining			=	(Button)findViewById(R.id.dealDining);
		bBeauty 		= 	(Button)findViewById(R.id.dealBueaty);
		bElectronics 	= 	(Button)findViewById(R.id.dealElectronics);
		bHealth 		= 	(Button)findViewById(R.id.dealHealth);
		bHosing 		= 	(Button)findViewById(R.id.dealHousing);
		bLegalServices 	= 	(Button)findViewById(R.id.dealServices);
		bMiscellaneous 	= 	(Button)findViewById(R.id.dealMiscellaneous);
		bReaction 		= 	(Button)findViewById(R.id.dealRecreation);
		bAllCategories 	= 	(Button)findViewById(R.id.dealAllCategories);
		
		pb_deals		=	(ProgressBar)findViewById(R.id.progressBarDeals);
		
	}
	private void setListner(){
		bAuto.setOnClickListener(this);
  	  	bNightLife.setOnClickListener(this);
  	  	bBeauty.setOnClickListener(this);
  	  	bElectronics.setOnClickListener(this);
  	  	bHealth.setOnClickListener(this);
  	  	bHosing.setOnClickListener(this);
  	  	bLegalServices.setOnClickListener(this);
  	  	bMiscellaneous.setOnClickListener(this);
  	  	bReaction.setOnClickListener(this);
  	  	bAllCategories.setOnClickListener(this);
  	  	bDining.setOnClickListener(this);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Big Deals");
		super.setBaseLayout(R.layout.activity_deals);	
		Registerwidgets();
		sendJsonRequestLoopi();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.dealAuto:
			Intent intent_deal_auto=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_auto=new Bundle();
			bundle_auto.putString("coupen_id",findCategoryId("Auto"));
			bundle_auto.putString("coupen_type",findCategoryTpye("Auto"));
			bundle_auto.putString("coupen_name","Auto");
			intent_deal_auto.putExtras(bundle_auto) ;
			startActivity(intent_deal_auto);
			break;
		case R.id.dealBueaty:	
			Intent intent_deal_bueaty=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_bueaty =new Bundle();
			bundle_bueaty.putString("coupen_id",findCategoryId("Beauty"));
			bundle_bueaty.putString("coupen_type",findCategoryTpye("Beauty"));
			bundle_bueaty.putString("coupen_name","Beauty");
			intent_deal_bueaty.putExtras(bundle_bueaty) ;
			startActivity(intent_deal_bueaty);
			break;
		case R.id.dealDining:	
			Intent intent_deal_dining=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_dining =new Bundle();
			bundle_dining.putString("coupen_id",findCategoryId("Dining"));
			bundle_dining.putString("coupen_type",findCategoryTpye("Dining"));
			bundle_dining.putString("coupen_name","Dining");
			intent_deal_dining.putExtras(bundle_dining) ;
			startActivity(intent_deal_dining);
			break;
		case R.id.dealElectronics:	
			Intent intent_deal_electronics=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_electronics =new Bundle();
			bundle_electronics.putString("coupen_id",findCategoryId("Electronics"));
			bundle_electronics.putString("coupen_type",findCategoryTpye("Electronics"));
			bundle_electronics.putString("coupen_name","Electronics");
			intent_deal_electronics.putExtras(bundle_electronics) ;
			startActivity(intent_deal_electronics);
			break;	
		case R.id.dealHealth:	
			Intent intent_health =new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_health =new Bundle();
			bundle_health.putString("coupen_id",findCategoryId("Health"));
			bundle_health.putString("coupen_type",findCategoryTpye("Health"));
			bundle_health.putString("coupen_name","Health");
			intent_health.putExtras(bundle_health) ;
			startActivity(intent_health);
			break;
		case R.id.dealHousing:	
			Intent intent_deal_housing=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_housing =new Bundle();
			bundle_housing.putString("coupen_id",findCategoryId("Housing"));
			bundle_housing.putString("coupen_type",findCategoryTpye("Housing"));
			bundle_housing.putString("coupen_name","Housing");
			intent_deal_housing.putExtras(bundle_housing) ;
			startActivity(intent_deal_housing);
			break;	
		case R.id.dealMiscellaneous:	
			Intent intent_deal_mis=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_mis =new Bundle();
			bundle_mis.putString("coupen_id",findCategoryId("Miscellaneous"));
			bundle_mis.putString("coupen_type",findCategoryTpye("Miscellaneous"));
			bundle_mis.putString("coupen_name","Miscellaneous");
			intent_deal_mis.putExtras(bundle_mis) ;
			startActivity(intent_deal_mis);
			break;
		case R.id.dealNightLife:	
			Intent intent_deal_night=new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_night =new Bundle();
			bundle_night.putString("coupen_id",findCategoryId("Nightlife"));
			bundle_night.putString("coupen_type",findCategoryTpye("Nightlife"));
			bundle_night.putString("coupen_name","Nightlife");
			intent_deal_night.putExtras(bundle_night) ;
			startActivity(intent_deal_night);
			break;	
		case R.id.dealRecreation:	
			Intent intent_recreation =new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_recreation =new Bundle();
			bundle_recreation.putString("coupen_id",findCategoryId("Recreation"));
			bundle_recreation.putString("coupen_type",findCategoryTpye("Recreation"));
			bundle_recreation.putString("coupen_name","Recreation");
			intent_recreation.putExtras(bundle_recreation) ;
			startActivity(intent_recreation);
			break;
		case R.id.dealServices:	
			Intent intent_deal_services =new Intent(Activity_Deals.this,Activity_Deal_Category.class);
			Bundle bundle_services =new Bundle();
			bundle_services.putString("coupen_id",findCategoryId("Services"));
			bundle_services.putString("coupen_type",findCategoryTpye("Services"));
			bundle_services.putString("coupen_name","Services");
			intent_deal_services.putExtras(bundle_services) ;
			startActivity(intent_deal_services);
			break;	
		case R.id.dealAllCategories:
			Intent all_category_intent = new Intent(Activity_Deals.this,Activity_All_Category.class);
			startActivity(all_category_intent);
		default:
			break;

		}
	}
	private void sendJsonRequestLoopi(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_deals, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
	 						JSONArray jArray1=object.getJSONArray("coupons_category");
	 						for(int i=0;i<jArray1.length();i++){
	 							Coupens coupensObject = new Coupens();
	 							JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							coupensObject.type				=	jobject1.getString("type");
	 							coupensObject.coupon_category	=	jobject1.getString("coupon_category");
	 							coupensObject.coupon_category_id=	jobject1.getString("coupon_category_id"); 
	 							arrCoupens.add(coupensObject);
	 						}
	 						pb_deals.setVisibility(View.INVISIBLE);
	 						setListner();
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	 pb_deals.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Deals.this);
					
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	private String findCategoryId(String btnTxt){
		for(int i=0;i<arrCoupens.size();i++){
			if(arrCoupens.get(i).coupon_category.contains(btnTxt)){
				return arrCoupens.get(i).coupon_category_id;
			}
		}
		return null;
	}
	private String findCategoryTpye(String btnTxt){
		for(int i=0;i<arrCoupens.size();i++){
			if(arrCoupens.get(i).coupon_category.contains(btnTxt)){
				return arrCoupens.get(i).type;
			}
		}
		return null;
	}


	

}

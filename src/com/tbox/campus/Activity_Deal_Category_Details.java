package com.tbox.campus;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;


public class Activity_Deal_Category_Details extends Activity_Base implements OnClickListener{
	ImageView	iv_main,website_btn,have_five_btn,navi_btn,call_btn,iv_call;
	TextView 	tv_coupen_title,tv_coupen_place,tv_coupen_desc,tv_coupen_title_image,
					tv_coupen_place_image,tv_coupen_high_five_deal_image,high_five_message,
					tv_phone_no , tv_website , tv_address;
	String 		coupons_company_website,coupons_company_phone_No,coupen_id,strPhone;
	ProgressBar	progressBar;
	int value,update_deal_data;
	int values=0;
	ProgressDialog 		mProgressDialog;
	String strName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,false,"");
		super.setBaseLayout(R.layout.activity_read_deal_detail);
		Registerwidgets();
		JSONObject 	jsonObj = null;
		try {
			jsonObj = new JSONObject(getIntent().getStringExtra("jsonObject"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bundle extras = getIntent().getExtras(); 
		value = extras.getInt("status");//Extra("jsonObject")
		update_deal_data = extras.getInt("value");
		tv_coupen_desc.setMovementMethod(new ScrollingMovementMethod());
//		Registerwidgets();
		if(jsonObj!=null)
			mainMenuSetting(jsonObj);
	}
	private void Registerwidgets() {
//		tv_news_detail = (TextView) findViewById(R.id.news_detail);
		iv_main = (ImageView) findViewById(R.id.ivDealDetails);
		tv_coupen_title=(TextView)findViewById(R.id.tvCoupenTitleDeal);
		tv_coupen_place=(TextView)findViewById(R.id.tvCoupenPlaceDeal);
		tv_coupen_desc=(TextView)findViewById(R.id.tvCoupenDescriptionDeal);
		website_btn=(ImageView)findViewById(R.id.webSiteDealDetail);
		call_btn=(ImageView)findViewById(R.id.callDealDetail);
		navi_btn=(ImageView)findViewById(R.id.naviDealDetail);
		have_five_btn=(ImageView)findViewById(R.id.highFiveDealDetail);
		tv_coupen_title_image=(TextView)findViewById(R.id.tvCoupenTitleDealImage);
		tv_coupen_place_image=(TextView)findViewById(R.id.tvCoupenPlaceDealImage);
		tv_coupen_high_five_deal_image=(TextView)findViewById(R.id.tvCoupenHighFiveDealImage);
		progressBar=(ProgressBar)findViewById(R.id.progressBarReadDealDetail);
		high_five_message=(TextView)findViewById(R.id.highFiveMessage);
		tv_phone_no = (TextView)findViewById(R.id.phoneNumber);
		tv_website = (TextView)findViewById(R.id.webSite);
		tv_address = (TextView)findViewById(R.id.address);
		iv_call = (ImageView)findViewById(R.id.phoneIcon);
		
		iv_main.setOnClickListener(this);
		iv_call.setOnClickListener(this);
		website_btn.setOnClickListener(this);
		call_btn.setOnClickListener(this);
		navi_btn.setOnClickListener(this);
		have_five_btn.setOnClickListener(this);
		
		values = Integer.parseInt(getResources().getString(R.string.whichscreen));
	}
	private void mainMenuSetting(JSONObject jsonOject){
		
		ImageLoader iv_loader_obj	=	new ImageLoader(Activity_Deal_Category_Details.this);
		try {
			
 			if(values==1080){
 				strName = jsonOject.getString("coupon_image");
 				iv_loader_obj.DisplayImage(strName, iv_main);
 			}else if(values==480){
 				strName = imagePath(jsonOject.getString("coupon_image"),"_hdpi_480");
 				iv_loader_obj.DisplayImage(strName, iv_main);
 			}else if(values==720){
 				strName = imagePath(jsonOject.getString("coupon_image"),"_xhdpi");
 				iv_loader_obj.DisplayImage(strName, iv_main);
 			}else if(values==600){
 				strName = imagePath(jsonOject.getString("coupon_image"),"_tab7");
 				iv_loader_obj.DisplayImage(strName, iv_main);
 			}else if(values==800){
 				strName = imagePath(jsonOject.getString("coupon_image"),"_tab10");
 				iv_loader_obj.DisplayImage(strName, iv_main);
 			}
//			iv_loader_obj.DisplayImage(jsonOject.getString("coupon_image"), iv_main);
//			if(!jsonOject.getString("coupon_image")){
//			new DownloadImage().execute(jsonOject.getString("coupon_image"));
			tv_coupen_title.setText(jsonOject.getString("coupon_title"));
			tv_coupen_place.setText(jsonOject.getString("place_at"));
			tv_coupen_desc.setText(jsonOject.getString("description"));
			strPhone =	jsonOject.getString("company_phone_number");
			coupons_company_website="http://"+jsonOject.getString("coupons_company_website");
			coupons_company_phone_No="tel:" +jsonOject.getString("company_phone_number");
			tv_coupen_title_image.setText(jsonOject.getString("coupon_title"));
			tv_coupen_place_image.setText(jsonOject.getString("place_at"));
			tv_coupen_high_five_deal_image.setText(jsonOject.getString("high_five"));
			coupen_id=jsonOject.getString("coupon_id");
			tv_phone_no.setText(jsonOject.getString("company_phone_number"));
			tv_address.setText(jsonOject.getString("business_adress"));
			
			tv_website.setText(Html.fromHtml(
		            " <font color='blue'><u><a href=\""+"http://"+jsonOject.getString("coupons_company_website")+"\">"+jsonOject.getString("coupons_company_website") +"</a></u></font>"));
			tv_website.setMovementMethod(LinkMovementMethod.getInstance());
			
//			tv_website.setText(jsonOject.getString("website_address"));
			
//			tv_address.setText(jsonOject.getString("group_name"));
//			tv_phone_no.setText(jsonOject.getString("phone_number"));
//			tv_website.setText(jsonOject.getString("website_address"));
//			iv_greek_details_subheading.setText(jsonOject.getString("group_name"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.ivDealDetails:
				Intent intent 	=	new Intent(Activity_Deal_Category_Details.this,Activity_Deal_Image.class);
				Bundle bundle	=	new Bundle();
				bundle.putString("imagePath", strName);
				intent.putExtras(bundle);
				startActivity(intent);
				break;
			case R.id.callDealDetail:
				Intent intent_call = new Intent(Intent.ACTION_CALL);
				intent_call.setData(Uri.parse(coupons_company_phone_No));
				startActivity(intent_call);
				break;
			case R.id.webSiteDealDetail:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(coupons_company_website));
			    startActivity(browserIntent);
				break;
			case R.id.naviDealDetail:
				
				break;
			case R.id.highFiveDealDetail:
				if(value==0){
					showAlertDialog("Please login for High Five..!");
				}else{
					progressBar.setVisibility(View.VISIBLE);
					highFiveHitsStatus();
					high_five_message.setVisibility(View.INVISIBLE);
				}
				break;
			case R.id.phoneIcon:
				 String uri = "tel:" +strPhone;
				 Intent intent_call_home = new Intent(Intent.ACTION_CALL);
				 intent_call_home.setData(Uri.parse(uri));
				 startActivity(intent_call_home);
				 break;
		}
	}
	private void highFiveHitsStatus(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
//		String id=""+settings.getInt("schoolid", 0);
		params.put("coupon_id",coupen_id);
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_high_five, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try{
					status = object.getInt("status");
					if(status ==1){ 
						progressBar.setVisibility(View.INVISIBLE);
//						String strValue = object.getInt("high_five");
						tv_coupen_high_five_deal_image.setText(object.getString("high_five"));
						high_five_message.setText(object.getString("success_message"));
						changeList(coupen_id,object.getString("high_five"));
					}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
//             @Override
//             protected void handleFailureMessage(Throwable e, String responseBody) {
//            	progressBar.setVisibility(View.INVISIBLE);
// 				try{
//					JSONObject jobject =  new JSONObject(responseBody);
//					int status = jobject.getInt("status");
//					if(status ==0){
////						UserInfo userInfo= UserInfo.getInstance(context);	
//						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Deal_Category_Details.this);
////						userInfo.saveUserInfo(context);
//					}
//				} catch (JSONException ex) {
//					// TODO Auto-generated catch block
//					ex.printStackTrace();
//				}
//
//             }
    	});
    	


	}
    private void showAlertDialog(String message)
	{
		new AlertDialog.Builder(Activity_Deal_Category_Details.this)
		.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Activity_Register.activity_base.finish();
				Intent intent_register = new Intent(Activity_Deal_Category_Details.this,Activity_Register.class);
				startActivity(intent_register);	
				finish();
			}
		})
		.show();
	}
    public void changeList(String coupen_id,String  high_five){
    	for(int i=0;i<Activity_Deal_Category.json_array_list.size();i++){
    		JSONObject 	jsonObj = Activity_Deal_Category.json_array_list.get(i);
    		try {
				if(jsonObj.getString("coupon_id").equals(coupen_id)){
					jsonObj.remove("high_five");
					jsonObj.put("high_five", high_five);
					if(update_deal_data==1)
						Activity_Deal_Category.json_array_list.set(i, jsonObj);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    private String imagePath(String imagePath , String addPath){
		String first = imagePath.substring(0, imagePath.length() -4);
		String second = imagePath.substring(imagePath.length()-4);			
		return first+addPath+second;
	}
	 // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Activity_Deal_Category_Details.this);
            // Set progressdialog title
//            mProgressDialog.setTitle("Download Image Tutorial");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }
 
        @Override
        protected Bitmap doInBackground(String... URL) {
        	File root = android.os.Environment.getExternalStorageDirectory();               

            File dir = new File (root.getAbsolutePath() + "/xmls");
            if(dir.exists()==false) {
                 dir.mkdirs();
            }

            File file = new File(dir, "faro.png");
            String imageURL = URL[0];
            BitmapFactory.Options options = new BitmapFactory.Options();
 
            Bitmap bitmap = null;
            URL myFileUrl = null;
            try {
            	  myFileUrl = new URL(imageURL);

                  HttpURLConnection conn= (HttpURLConnection)myFileUrl.openConnection();
                  conn.setDoInput(true);
                  conn.connect();
                  InputStream is = conn.getInputStream();
                  

                  BufferedInputStream bis = new BufferedInputStream(is);

                  ByteArrayBuffer baf = new ByteArrayBuffer(1000000);
                  int current = 0;
                  while ((current = bis.read()) != -1) {
                      baf.append((byte)current);
                  }
//                  byte[] imageData = baf.toByteArray();
                  
                  FileOutputStream fos = new FileOutputStream(file);
                  fos.write(baf.toByteArray());
                  fos.flush();
                  fos.close();
//                  BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
//                  options.inJustDecodeBounds = true;
//                  options.inSampleSize = 8; //calculateInSampleSize(options, 128, 128);
//                  options.inJustDecodeBounds = false;
//                  bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length, options);
//                  BitmapFactory.Options options=new BitmapFactory.Options();
//                  options.inSampleSize = 16;
//
//                  bitmap = BitmapFactory.decodeStream(is, null, options);
	            	
            	
            	
                // Download Image from URL
//                InputStream input = new java.net.URL(imageURL).openStream();
//                // Decode Bitmap
//                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
 
        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
        	iv_main.setImageBitmap(result);
            // Close progressdialog
            mProgressDialog.dismiss();
        }
    }



}

package com.tbox.campus;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;



public class Activity_Register extends Activity_Base implements OnClickListener {
//	private static final int RESULT_CLOSE_ALL = 0;
	Button 		btn_register;
	EditText 	et_fname,et_lname,et_email,et_password;
	RadioButton rb_male,rb_female;
	ProgressBar pb_registration;
	String 		gender="";
	int 		status;
	Context 	context;
	TextView 	tv_link;
	ImageLoader 	imageLoader;
	Timer timer;
	TimerTask timertask;
	ScrollViewNoScroll bannerScrollbar;
	ArrayList<JSONObject> json_array_list;
	ArrayList<String> coupens_image;
	static Activity_Base activity_base;
@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,false,"");
		super.setBaseLayout(R.layout.activity_register);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		context=this;
		activity_base=this;
		registerWidgets();
		setListener();
		setHyperLink();
		pb_registration.setVisibility(View.VISIBLE);
		getBannerImagesAndData();
	}
	private void setHyperLink(){
	      //Seting Hyperlink in TextView 
	      tv_link.setText(Html.fromHtml(
		            " <font color='black'><u><a href=\"http://www.google.com\">Terms of Services</a></u></font>" +
		            "  and  " +
		            "<font color='black'><u><a href=\"http://www.google.com\">Privacy Policy</a></u></font>"));
	      tv_link.setMovementMethod(LinkMovementMethod.getInstance());

	}
	private void setListener(){
		btn_register.setOnClickListener(this);
		rb_male.setOnClickListener(this);
		rb_female.setOnClickListener(this);
		pb_registration.setVisibility(View.INVISIBLE);
		
		rb_male.setChecked(true);
		gender="0";
	}
	
	private void registerWidgets(){
		  
		  btn_register	=	(Button)findViewById(R.id.register);
		  et_fname		=	(EditText)findViewById(R.id.fname);
		  et_lname		=	(EditText)findViewById(R.id.lname);
		  et_email		=	(EditText)findViewById(R.id.email);
		  et_password	=	(EditText)findViewById(R.id.password);
	      rb_male		=	(RadioButton)findViewById(R.id.male);
	      rb_female		=	(RadioButton)findViewById(R.id.female);
	      pb_registration = (ProgressBar)findViewById(R.id.progressBarRegistration);
	      tv_link		= 	(TextView)findViewById(R.id.registerTextClick);
	      imageLoader = new ImageLoader(getApplicationContext());
	      bannerScrollbar=(ScrollViewNoScroll)findViewById(R.id.bannerscrollViewRegistration);
	      json_array_list	= new ArrayList<JSONObject>();
	      coupens_image= new ArrayList<String>();
	      
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.register:
			if(Utils.isDataValid(et_email.getText().toString(),et_password.getText().toString(),Activity_Register.this)){
				pb_registration.setVisibility(View.VISIBLE);
				sendRequestJsonRegistration();
			}
			break;
		case R.id.male:
			rb_male.setChecked(true);
		    rb_female.setChecked(false);
            gender="0";
			break;
         
		case R.id.female:
			rb_female.setChecked(true);
		    rb_male.setChecked(false);
		    gender="1";
			break;        
		default:
			break;
		}
	}
	private void sendRequestJsonRegistration(){
		
		JSONObject json = new JSONObject();
	    try {
			json.put("password",et_password.getText().toString());
			json.put("first_name",et_fname.getText().toString());
			json.put("last_name",et_lname.getText().toString());
			json.put("email_id",et_email.getText().toString());
			if(rb_male.isChecked())
				json.put("gender",gender);
			else if(rb_female.isChecked())
				json.put("gender",gender);
			
		}catch (JSONException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RequestParams params = new RequestParams();
	    params.put("data", json.toString());

    	AsyncHttpClient client = new AsyncHttpClient();
    	client.post(Constant.addess_url_base+Constant.address_register, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					if(status == 1){            			
						UserInfo userInfoData= UserInfo.getInstance(Activity_Register.this);
						userInfoData.first_name = et_fname.getText().toString();
						userInfoData.last_name = et_lname.getText().toString();
						userInfoData.email_id = et_email.getText().toString();
						userInfoData.password = et_password.getText().toString();
						userInfoData.gender = gender;
						userInfoData.noti_email = 0;
						userInfoData.noti_push_woosh = 0;
						SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				 		SharedPreferences.Editor editor = settings.edit();
						try {
							userInfoData.profile_image_path = object.getString("place_holder");
							userInfoData.api_key = object.getString("api_key");
							editor.putString("key",object.getString("api_key")).commit();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
						userInfoData.saveUserInfo(Activity_Register.this);
						openStateActivity();								
					}else
						Utils.showAlertDialog("you don not  have sign in....!",Activity_Register.this);
            	 

             }
           });
	}
	void openStateActivity(){
		pb_registration.setVisibility(View.INVISIBLE);
		Activity_Login.activity_base.finish();
		Intent openStateActivityIntent=new Intent(getApplicationContext(),Activity_Main_Fragment.class);
		openStateActivityIntent.putExtra("key", 1);
 		startActivity(openStateActivityIntent);
 		finish();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
		super.onBackPressed();
	}
	private void stopAndStartTimer(){
		timer.cancel();
		startTimer();
		
	}
	private void startTimer(){
		
		timer=new Timer();
		timertask = new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				runOnUiThread(new Runnable() {
					public void run() {
						try {
			        			int y = bannerScrollbar.getScrollY();
			        			Log.d("", ""+y);
			        			float yMod=(y+1)%220;
			        			if(yMod==0){
			        				bannerScrollbar.scrollTo(0, y+1);
			        				if((y+1)>=(coupens_image.size()*220)){
			        					bannerScrollbar.scrollTo(0, 0);
			        				}
			        				
			        				stopAndStartTimer();
			        			}else{
			        				bannerScrollbar.scrollTo(0, y+1);
			        			}
			        			
			        			
						
						} catch (NullPointerException e) {
							// TODO: handle exception
						}
						   	
					}
				});
				
			}
			
		};
		timer.schedule(timertask, 4000, 10);
	}
	private void setBannerView(){
		RelativeLayout bannerLayout = (RelativeLayout)findViewById(R.id.bannerLayout);
		bannerLayout.setVisibility(View.VISIBLE);
		Thread myThread = new Thread(new Runnable(){
	        
	    	   @SuppressLint("SdCardPath")
			@Override
	    	   public void run() {
				
				runOnUiThread(new Runnable(){

		    	     @Override
		    	     public void run() {
		    	    	 try {
		    	    		// Create a LinearLayout element
		    	    		 	pb_registration.setVisibility(View.INVISIBLE);
		    	    			LinearLayout linearLayout = new LinearLayout(Activity_Register.this);
		    	    			linearLayout.setOrientation(LinearLayout.VERTICAL);
		    	    			//add last image at the top of the scrollview 
		    	    			ImageView imgView1 = new ImageView(Activity_Register.this);
		    	    			imgView1.setScaleType(ImageView.ScaleType.FIT_XY);
		    	    			imgView1.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub

										Intent intent 	=	new Intent(Activity_Register.this,Activity_Deal_Category_Details.class);
										Bundle bundle	=	new Bundle();
										bundle.putString("jsonObject", json_array_list.get((Integer) v.getTag()).toString());
										bundle.putInt("status", 0);
										bundle.putInt("value", 0);
										intent.putExtras(bundle) ;
										startActivity(intent);	

									}
								});
		    	    			imageLoader.DisplayImage(coupens_image.get(coupens_image.size()-1), imgView1);
		    	    			
		    	    			imgView1.setTag((coupens_image.size()-1));
			    	    		imgView1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 220));
			    	    			linearLayout.addView(imgView1);
		    	    	 for(int i=0;i<coupens_image.size();i++)
		    	    	 {
		    	    		 ImageView imgView = new ImageView(Activity_Register.this);
		    	    		 imgView.setTag(i);
		    	    		 imgView.setScaleType(ImageView.ScaleType.FIT_XY);
		    	    		 imgView.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
	
										Intent intent 	=	new Intent(Activity_Register.this,Activity_Deal_Category_Details.class);
										Bundle bundle	=	new Bundle();
										bundle.putString("jsonObject", json_array_list.get((Integer) v.getTag()).toString());
										bundle.putInt("status", 0);
										bundle.putInt("value", 0);
										intent.putExtras(bundle) ;
										startActivity(intent);	

									}
								});
		    	    		 imageLoader.DisplayImage(coupens_image.get(i), imgView);
		    	    		 imgView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 220));
		    	    		 linearLayout.addView(imgView);
		    	    		 
		    	    	 }
		    	    	 bannerScrollbar.addView(linearLayout);
		    	    	 startTimer();
		    	    	 } catch (Exception e) {
								// TODO: handle exception
		    	    		 e.printStackTrace();
		    	    	 }
		    	     }});
	    	   }
		});
		myThread.start();
	}

	private void getBannerImagesAndData(){
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_sign_coupens, new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
	 						JSONArray jArray1=object.getJSONArray("coupons");
	 						for(int i=0;i<jArray1.length();i++){
	 							JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							coupens_image.add(jobject1.getString("coupon_image"));
	 							json_array_list.add(jobject1);
	 						}
	 						setBannerView();
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_registration.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Register.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}	

//	//* return from gallery and save image in imageview of Register Activity  *//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	   super.onActivityResult(requestCode, resultCode, data);
//	   if (resultCode == RESULT_OK && requestCode == 1) {
//	      setResult(RESULT_OK);
//	      finish();
//	   }
//	}

}	


package com.tbox.campus;

import java.util.ArrayList;
import java.util.List;
import com.tbox.classes.*;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_News extends Activity_Base implements OnClickListener{
	Button 					bCampusNew , bExams , bSportsAndClubs , bVolunters , bEmployment , bClassified , 
								bCnnNews , bBccNews , bAssociatedPress;
	int 					status;
	JSONArray 				jArray;
	List<NameValuePair> 	userInfo;
	ProgressBar 			pb_get_news;
	ArrayList<News> 		newArrayList = new ArrayList<News>();
	EditText 				et_Search;
	ArrayList<SearchData> 	searchListView;

	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"News");
		super.setBaseLayout(R.layout.activity_news);
		Registerwidgets();
		sendJsonRequestLoopi();
	}
	private void Registerwidgets(){
		bCampusNew	=		(Button)findViewById(R.id.campusNews);
  	  	bExams	=			(Button)findViewById(R.id.exams);
  	  	bSportsAndClubs=	(Button)findViewById(R.id.sportsAndClubs);
  	  	bVolunters	=		(Button)findViewById(R.id.volunters);
  	  	bEmployment	=		(Button)findViewById(R.id.Employment);
  	  	bClassified	=		(Button)findViewById(R.id.Classified);
  	  	pb_get_news	=		(ProgressBar)findViewById(R.id.progressBarGetNews);
  	  	bCnnNews	=		(Button)findViewById(R.id.cnnNews);
  	  	bBccNews	=		(Button)findViewById(R.id.bbcNews);
  	  	bAssociatedPress=	(Button)findViewById(R.id.associatedPress);
  	  	
  	  	searchListView	=	new ArrayList<SearchData>();
  	}
	private void setListner(){
	    bCampusNew.setOnClickListener(this);
	    bExams.setOnClickListener(this);
	    bSportsAndClubs.setOnClickListener(this);
	    bVolunters.setOnClickListener(this);
	    bEmployment.setOnClickListener(this);
	    bClassified.setOnClickListener(this);
	    bCnnNews.setOnClickListener(this);
	    bBccNews.setOnClickListener(this);
	    bAssociatedPress.setOnClickListener(this);
    }
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.campusNews:
				Intent intent_campus_new=new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_campus=new Bundle();
				bundle_campus.putString("category_id",findCategoryId("New"));
				bundle_campus.putString("category_type","New");
				intent_campus_new.putExtras(bundle_campus) ;
				startActivity(intent_campus_new);
				break;
			case R.id.exams:	
				Intent intent_exams=new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_exam =new Bundle();
				bundle_exam.putString("category_id",findCategoryId("Exam"));
				bundle_exam.putString("category_type","Exam");
				intent_exams.putExtras(bundle_exam) ;
				startActivity(intent_exams);
				break;
			case R.id.sportsAndClubs:	
				Intent intent_sports=new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_sports =new Bundle();
				bundle_sports.putString("category_id",findCategoryId("Sports"));
				bundle_sports.putString("category_type","Sports");
				intent_sports.putExtras(bundle_sports) ;
				startActivity(intent_sports);
				break;
			case R.id.volunters:	
				Intent intent_volunters=new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_volunters =new Bundle();
				bundle_volunters.putString("category_id",findCategoryId("Volenteer"));
				bundle_volunters.putString("category_type","Volenteer");
				intent_volunters.putExtras(bundle_volunters) ;
				startActivity(intent_volunters);
				break;	
			case R.id.Employment:	
				Intent intent_employment =new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_employment =new Bundle();
				bundle_employment.putString("category_id",findCategoryId("Employment"));
				bundle_employment.putString("category_type","Employment");
				intent_employment.putExtras(bundle_employment) ;
				startActivity(intent_employment);
				break;
			case R.id.Classified:	
				Intent intent_classified=new Intent(Activity_News.this,Activity_News_Detail.class);
				Bundle bundle_classified =new Bundle();
				bundle_classified.putString("category_id",findCategoryId("Classi"));
				bundle_classified.putString("category_type","Classi");
				intent_classified.putExtras(bundle_classified) ;
				startActivity(intent_classified);
				break;
			case R.id.cnnNews:
				Intent browserIntentCnn = new Intent(Intent.ACTION_VIEW, Uri.parse("http://edition.cnn.com/"));
			    startActivity(browserIntentCnn);
				break;
			case R.id.bbcNews:
				Intent browserIntentBbc = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.bbc.com/"));
			    startActivity(browserIntentBbc);
				break;
			case R.id.associatedPress:
				Intent browserIntentAssociated = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ap.org/"));
			    startActivity(browserIntentAssociated);
				break;
			default:
				break;
 		}
		
	}
	void sendJsonRequestLoopi(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_news, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
	 						JSONArray jArray1=object.getJSONArray("categories");
	 						for(int i=0;i<jArray1.length();i++){
	 							News newObject = new News();
	 							JSONObject	jobject1 = jArray1.getJSONObject(i);
	 							newObject.category_id	=	jobject1.getString("news_category_id");
	 				            newObject.newType		=	jobject1.getString("news_category_name"); 
	 				            newArrayList.add(newObject);
	 						}
	 						pb_get_news.setVisibility(View.INVISIBLE);
	 						setListner();
//	 						mainMenuSetting();
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_get_news.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_News.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	String findCategoryId(String btnTxt){
		for(int i=0;i<newArrayList.size();i++){
			if(newArrayList.get(i).newType.contains(btnTxt)){
				return newArrayList.get(i).category_id;
			}
		}
		return null;
	}
	

}

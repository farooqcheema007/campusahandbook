package com.tbox.campus;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.campus.R;

public class Activity_Greek extends Activity_Base implements OnClickListener{
	
	Button 		btn_fraternities , btn_sororities;
	
	
	private void Registerwidgets(){
		btn_fraternities	=	(Button)findViewById(R.id.btn_fraternities);
		btn_sororities		=	(Button)findViewById(R.id.btn_sororities);
	}
	private void setListner(){
		 btn_fraternities.setOnClickListener(this);
		 btn_sororities.setOnClickListener(this);
	 }
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Greek Life");
		super.setBaseLayout(R.layout.activity_greek);
		Registerwidgets();
		setListner();

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.btn_fraternities:
				Intent fraternities_intent =new Intent(Activity_Greek.this,Activity_Fraternity.class);
				startActivity(fraternities_intent);
				break;
			case R.id.btn_sororities:
				Intent sororities_intent =new Intent(Activity_Greek.this,Activity_Sorority.class);
				startActivity(sororities_intent);
				break;
	
			
		}
	}
}

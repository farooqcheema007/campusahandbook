package com.tbox.campus;

public class Constant {
	static String addess_url_base = 		"http://mfaenergy.com/campus_hb_dev/api/";
	static String addess_map = 				"get_school_map";
	static String address_contacts = 		"get_contacts";
	static String address_search = 			"search_coupons";
	static String address_image_uploading = "update_profile_image/";
	static String address_login = 			"login";
	static String address_register =		"register_user";
	static String address_states = 			"get_states";
	static String address_profile_update =  "edit_profile";
	static String address_news = 			"get_news_categories";
	static String address_news_detail =		"get_news";
	static String address_fraternities=		"get_fraternities";
	static String address_sorority=			"get_sororities";
	static String address_deals=			"get_coupons_category_hc";
	static String address_deals_data=		"get_coupons";
	static String address_party=			"get_party_categories";
	static String address_high_five=		"coupon_high_five";
	static String address_party_details=	"get_parties";
	static String addres_party_high_five=	"party_high_five";
	static String address_add_event=		"add_event_data";
	static String address_add_event_image=	"add_event_image";
	static String address_add_news_coupens=	"get_news_coupons";
	static String address_sign_coupens=		"signup_coupon";
	static String address_deal_of_week=		"get_deal_of_the_week";
	static String address_all_category_deal="get_coupons";
	static String address_setting_noti =    "update_notification";
	static String address_greek_high_five = "group_high_five";
	}

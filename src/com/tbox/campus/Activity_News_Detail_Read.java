package com.tbox.campus;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.campus.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.SessionStore;
import com.google.android.gms.plus.PlusShare;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.campus.TwitterApp.TwDialogListener;
import com.tbox.imagedownload.ImageLoader;


public class Activity_News_Detail_Read extends Activity_Base implements OnClickListener {
	
//	private static final String URL = "https://developers.google.com/+";
	// The request code must be 0 or higher.
//	private static final int PLUS_ONE_REQUEST_CODE = 0;
	Button gPlus;
	Facebook mFacebook;
	ToggleButton mFacebookBtn;
	ProgressDialog mProgress;
	private static final String APP_ID = "197020810471914";
	private static final String[] PERMISSIONS = new String[] {
		"publish_stream", "read_stream", "offline_access" };
	Bundle 				bundle;
	TextView 			tv_news_detail,tv_news_title;
	ImageView 			iv_news_image;
	ImageLoader 		imageLoader;
	Timer 				timer;
	TimerTask 			timertask;
	ScrollViewNoScroll 	bannerScrollbar;
	ProgressBar pb;
	
	ArrayList<JSONObject> json_array_list;
	ArrayList<String> coupens_image;

	private TwitterApp mTwitter;
	private ToggleButton mTwitterBtn;

	private static final String twitter_consumer_key = "m1lV7zrCjAUGxb97keXYUg";
	private static final String twitter_secret_key = "C9nIXsNo3lLTskkJoNqI2Yn2b8ThYmwbNxVrDCBoVzQ";
	
//	private Facebook mFacebookTest;
//	private CheckBox mFacebookCb;
//	private ProgressDialog mProgressTest;
	
	private Handler mRunOnUi = new Handler();
	
//	private static final String APP_ID = "197020810471914";

	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,false,"");
		super.setBaseLayout(R.layout.activity_read_news_detail);
		Registerwidgets();
		bundle = getIntent().getExtras();
		getBannerImagesAndData();

		/*
		 * //Image_Download imageDownload= new
		 * Image_Download(bundle.getString("news_image"),"newsimage.jpg");
		 * String filepath="";
		 * 
		 * try { filepath = imageDownload.execute().get(); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (ExecutionException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * Bitmap bitmap = BitmapFactory.decodeFile(filepath);
		 */
		// news_image.setImageBitmap(bitmap);
		imageLoader.DisplayImage(bundle.getString("news_image"), iv_news_image);
		tv_news_detail.setText(bundle.getString("news_description"));
		tv_news_title.setText(bundle.getString("news_title"));
		
		tv_news_detail.setMovementMethod(new ScrollingMovementMethod());
		
		mFacebookBtn = (ToggleButton) findViewById(R.id.faceBookNews);
		
		
		//facebok integration.....
		
		mProgress = new ProgressDialog(this);
		mFacebook = new Facebook("197020810471914");

		SessionStore.restore(mFacebook, this);

		if (mFacebook.isSessionValid()) {
			mFacebookBtn.setChecked(true);

			String name = SessionStore.getName(this);
			name = (name.equals("")) ? "Unknown" : name;

			
			mFacebookBtn.setTextColor(Color.WHITE);
		}

		mFacebookBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onFacebookClick();
			}
		});
		//gplus integration happen
		gPlus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		        Intent shareIntent = new PlusShare.Builder()
	            .setType("text/plain")
	            .setText("Welcome to The Campus HandBook.")
	            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.tboxsolutionz.mobitorch&hl=en"))
	            .getIntent();

	        startActivityForResult(shareIntent, 0);
			}
		});
		
		//twitter integration....... 
		
		mTwitterBtn.setOnClickListener(new OnClickListener() {
			   @Override
			   public void onClick(View v) {
			    onTwitterClick();
			   }
			  });

			  mTwitter = new TwitterApp(this, twitter_consumer_key,
			    twitter_secret_key);

			  mTwitter.setListener(mTwLoginDialogListener);

			  if (mTwitter.hasAccessToken()) {
			   mTwitterBtn.setChecked(true);

			   String username = mTwitter.getUsername();
			   

			   
			   mTwitterBtn.setTextColor(Color.WHITE);
			  }
		
	}

	private void Registerwidgets() {
		tv_news_detail = (TextView) findViewById(R.id.news_detail);
		tv_news_title=(TextView)findViewById(R.id.news_detail_title);
		iv_news_image = (ImageView) findViewById(R.id.news_image);
		bannerScrollbar=(ScrollViewNoScroll)findViewById(R.id.bannerscrollView1);
		json_array_list	= new ArrayList<JSONObject>();
		coupens_image= new ArrayList<String>();
		imageLoader = new ImageLoader(getApplicationContext());
		gPlus = (Button) findViewById(R.id.gmailNewsFinal);
		pb = (ProgressBar)findViewById(R.id.progressBarNewsReadDetails);
		mTwitterBtn = (ToggleButton) findViewById(R.id.twiterNews);
		mProgress	= new ProgressDialog(this);
//		mFacebookTest 	= new Facebook("197020810471914");

	}
	private void stopAndStartTimer(){
		timer.cancel();
		startTimer();
		
	}
	private void startTimer(){
		
		timer=new Timer();
		timertask = new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				runOnUiThread(new Runnable() {
					public void run() {
						try {
			        			int y = bannerScrollbar.getScrollY();
			        			Log.d("", ""+y);
			        			float yMod=(y+1)%220;
			        			if(yMod==0){
			        				bannerScrollbar.scrollTo(0, y+1);
			        				if((y+1)>=(coupens_image.size()*220)){
			        					bannerScrollbar.scrollTo(0, 0);
			        				}
			        				
			        				stopAndStartTimer();
			        			}else{
			        				bannerScrollbar.scrollTo(0, y+1);
			        			}
			        			
			        			
						
						} catch (NullPointerException e) {
							// TODO: handle exception
						}
						   	
					}
				});
				
			}
			
		};
		timer.schedule(timertask, 4000, 10);
	}
	private void setBannerView(){
		RelativeLayout bannerLayout = (RelativeLayout)findViewById(R.id.bannerLayout);
		bannerLayout.setVisibility(View.VISIBLE);
		Thread myThread = new Thread(new Runnable(){
	        
	    	   @SuppressLint("SdCardPath")
			@Override
	    	   public void run() {
				
				runOnUiThread(new Runnable(){

		    	     @Override
		    	     public void run() {
		    	    	 try {
//		    	    		 Document linksPlistData = convertStringToDocument(readsdcardFiles( new File("/sdcard/Magzine/appBanner/links.plist")));
		    	    		// Create a LinearLayout element
		    	    		 	pb.setVisibility(View.INVISIBLE);
		    	    			LinearLayout linearLayout = new LinearLayout(Activity_News_Detail_Read.this);
		    	    			linearLayout.setOrientation(LinearLayout.VERTICAL);
		    	    			//add last image at the top of the scrollview 
		    	    			ImageView imgView1 = new ImageView(Activity_News_Detail_Read.this);
		    	    			imgView1.setScaleType(ImageView.ScaleType.FIT_XY);
		    	    			imgView1.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub

										Intent intent 	=	new Intent(Activity_News_Detail_Read.this,Activity_Deal_Category_Details.class);
										Bundle bundle	=	new Bundle();
										bundle.putInt("value", 0);
										bundle.putInt("status", 1);
										bundle.putString("jsonObject", json_array_list.get((Integer) v.getTag()).toString());
										intent.putExtras(bundle) ;
										startActivity(intent);	

									}
								});
		    	    			imageLoader.DisplayImage(coupens_image.get(coupens_image.size()-1), imgView1);
		    	    			
		    	    			imgView1.setTag((coupens_image.size()-1));
			    	    		imgView1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 220));
			    	    			linearLayout.addView(imgView1);
		    	    	 for(int i=0;i<coupens_image.size();i++)
		    	    	 {
		    	    		 ImageView imgView = new ImageView(Activity_News_Detail_Read.this);
		    	    		 imgView.setTag(i);
		    	    		 imgView.setScaleType(ImageView.ScaleType.FIT_XY);
		    	    		 imgView.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
	
										Intent intent 	=	new Intent(Activity_News_Detail_Read.this,Activity_Deal_Category_Details.class);
										Bundle bundle	=	new Bundle();
										bundle.putInt("status", 1);
										bundle.putInt("value", 0);
										bundle.putString("jsonObject", json_array_list.get((Integer) v.getTag()).toString());
										intent.putExtras(bundle) ;
										startActivity(intent);	

									}
								});
		    	    		 imageLoader.DisplayImage(coupens_image.get(i), imgView);
		    	    		 imgView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 220));
		    	    		 linearLayout.addView(imgView);
		    	    		 
		    	    	 }
		    	    	 bannerScrollbar.addView(linearLayout);
		    	    	 startTimer();
		    	    	 } catch (Exception e) {
								// TODO: handle exception
		    	    		 e.printStackTrace();
		    	    	 }
		    	     }});
	    	   }
		});
		myThread.start();
	}
	//Response for Add News
	private void getBannerImagesAndData(){
			RequestParams params = new RequestParams();
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
			String id=""+settings.getInt("schoolid", 0);
			params.put("school_id",id.toString());
			params.put("api_key",settings.getString("key", null).toString());
	        
	        //send get request for New Details
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.get(Constant.addess_url_base+Constant.address_add_news_coupens, params , new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
	            	int status = 0;
					try {
						status = object.getInt("status");
						 if(status ==1){ 
		 						JSONArray jArray1=object.getJSONArray("coupons");
		 						for(int i=0;i<jArray1.length();i++){
		 							JSONObject	jobject1 = jArray1.getJSONObject(i);
		 							coupens_image.add(jobject1.getString("coupon_image"));
		 							json_array_list.add(jobject1);
		 						}
		 						setBannerView();
//		 						pb_get_news.setVisibility(View.INVISIBLE);
//		 						setListner();
//		 						mainMenuSetting();
						 }
					
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
	            	
	             }
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
//	            	pb_get_news.setVisibility(View.INVISIBLE);
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0)
							Utils.showAlertDialog(jobject.getString("error_message"), Activity_News_Detail_Read.this);
						
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}

	             }
	    	});

	}
	//FaceBook Integration Methods//
	private void onFacebookClick() {
		if (mFacebook.isSessionValid()) {
			postToFacebook(bundle.getString("news_title"));
//			startActivity(new Intent(Activity_News_Detail_Read.this, TestPost.class));
		} else {
			mFacebookBtn.setChecked(false);

			mFacebook.authorize(this, PERMISSIONS, -1,
					new FbLoginDialogListener());
		}
	}

	private final class FbLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionStore.save(mFacebook, Activity_News_Detail_Read.this);
			
			mFacebookBtn.setChecked(true);
			mFacebookBtn.setTextColor(Color.WHITE);

			getFbName();
		}

		public void onFacebookError(FacebookError error) {
			Toast.makeText(Activity_News_Detail_Read.this, "Facebook connection failed",
					Toast.LENGTH_SHORT).show();

			mFacebookBtn.setChecked(false);
		}

		public void onError(DialogError error) {
			Toast.makeText(Activity_News_Detail_Read.this, "Facebook connection failed",
					Toast.LENGTH_SHORT).show();

			mFacebookBtn.setChecked(false);
		}

		public void onCancel() {
			mFacebookBtn.setChecked(false);
		}
	}

	private void getFbName() {
		mProgress.setMessage("Finalizing ...");
		mProgress.show();

		new Thread() {
			@Override
			public void run() {
				String name = "";
				int what = 1;

				try {
					String me = mFacebook.request("me");

					JSONObject jsonObj = (JSONObject) new JSONTokener(me)
							.nextValue();
					name = jsonObj.getString("name");
					what = 0;
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				mFbHandler.sendMessage(mFbHandler.obtainMessage(what, name));
			}
		}.start();
	}

	private Handler mFbHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			mProgress.dismiss();

			if (msg.what == 0) {
				String username = (String) msg.obj;
				username = (username.equals("")) ? "No Name" : username;

				SessionStore.saveName(username, Activity_News_Detail_Read.this);

//				mFacebookBtn.setText("  Facebook (" + username + ")");

				Toast.makeText(Activity_News_Detail_Read.this,
						"Connected to Facebook as " + username,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(Activity_News_Detail_Read.this, "Connected to Facebook",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			mProgress.dismiss();

			if (msg.what == 1) {
				Toast.makeText(Activity_News_Detail_Read.this, "Facebook logout failed",
						Toast.LENGTH_SHORT).show();
			} else {
				mFacebookBtn.setChecked(false);
				mFacebookBtn.setText("  Facebook (Not connected)");
				mFacebookBtn.setTextColor(Color.GRAY);

				Toast.makeText(Activity_News_Detail_Read.this, "Disconnected from Facebook",
						Toast.LENGTH_SHORT).show();
			}
		}
	};	
	
	private void onTwitterClick() {
		  if (mTwitter.hasAccessToken()) {
		   startActivity(new Intent(Activity_News_Detail_Read.this, TweetPost.class));
		  } else {
		   mTwitterBtn.setChecked(false);

		   mTwitter.authorize();
		  }
		 }

	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {
		  @Override
		  public void onComplete(String value) {
		   String username = mTwitter.getUsername();
		   username = (username.equals("")) ? "No Name" : username;

		   
		   mTwitterBtn.setChecked(true);
		   mTwitterBtn.setTextColor(Color.WHITE);

		   Toast.makeText(Activity_News_Detail_Read.this,
		     "Connected to Twitter as " + username, Toast.LENGTH_LONG)
		     .show();
		 }

	public void onError(String value) {
		  mTwitterBtn.setChecked(false);

		  Toast.makeText(Activity_News_Detail_Read.this,
		    "Twitter connection failed", Toast.LENGTH_LONG).show();
		  }
		 };
		 
		 private void postToFacebook(String review) {	
				mProgress.setMessage("Posting ...");
				mProgress.show();
				
				AsyncFacebookRunner mAsyncFbRunner = new AsyncFacebookRunner(mFacebook);
				iv_news_image.buildDrawingCache();
				Bitmap bmap = iv_news_image.getDrawingCache();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				
				Bundle params = new Bundle();
		    		
				params.putString("message", review);
				params.putString("name", review);
				params.putByteArray("image", byteArray);
				params.putString("caption", "By Talal Qaboos");
				params.putString("link", "http://www.tboxsolutions.com");
				params.putString("description", "Experts in iOS , Android, Web Development");
				params.putString("picture", "http://xbmc.org/wp-content/uploads/2012/12/zappy_android.jpg");
				
				mAsyncFbRunner.request("me/photos", params,"POST", new WallPostListener());
			}

			private final class WallPostListener extends BaseRequestListener {
		        public void onComplete(final String response) {
		        	mRunOnUi.post(new Runnable() {
		        		@Override
		        		public void run() {
		        			mProgress.cancel();
		        			Toast.makeText(Activity_News_Detail_Read.this, "Posted to Facebook", Toast.LENGTH_SHORT).show();
		        		}
		        	});
		        }
		   }	 

}

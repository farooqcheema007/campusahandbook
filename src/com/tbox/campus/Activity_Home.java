package com.tbox.campus;


import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.campus.R;
@SuppressLint("NewApi")
public class Activity_Home extends Fragment implements OnClickListener{
	Button 		btn_contacts,btn_events,btn_news,btn_maps,btn_calender,btn_coupons,btn_group,btn_weather,btn_navigation;//,btn_profile;
	TextView 	home;
  	EditText 	et_Search;
  	RelativeLayout rl = null;
	FrameLayout.LayoutParams menuPanelParameters;
	FrameLayout.LayoutParams slidingPanelParameters;
	LinearLayout.LayoutParams headerPanelParameters ;
	LinearLayout.LayoutParams listViewParameters;
	@SuppressLint("NewApi")
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		UserInfo userInfo = UserInfo.getInstance(Activity_Home.this);
//		super.setHeader(true,true,"Welcome " + userInfo.first_name);
//		super.setBaseLayout(R.layout.activity_homes);
//		Registerwidgets();
//		setListner();
//   	}
	
	private void Registerwidgets(View v){
    	  btn_contacts=(Button)v.findViewById(R.id.contact);
    	  btn_events=(Button)v.findViewById(R.id.party);
    	  btn_news=(Button)v.findViewById(R.id.news);
    	  btn_maps=(Button)v.findViewById(R.id.maps);
    	  btn_calender=(Button)v.findViewById(R.id.calender);
    	  btn_coupons=(Button)v.findViewById(R.id.deals);
    	  btn_group=(Button)v.findViewById(R.id.geek);
    	  btn_weather=(Button)v.findViewById(R.id.weather);
    	  btn_navigation=(Button)v.findViewById(R.id.navigation);
//    	  btn_setting=(Button)v.findViewById(R.id.settingBtn);
//    	  btn_profile=(Button)v.findViewById(R.id.profileBtn);
    	  rl=(RelativeLayout)v.findViewById(R.id.homeRelativeLayout);
    }
	private void setListner(){
    	  btn_contacts.setOnClickListener(this);
    	  btn_events.setOnClickListener(this);
    	  btn_news.setOnClickListener(this);
    	  btn_maps.setOnClickListener(this);
    	  btn_calender.setOnClickListener(this);
    	  btn_coupons.setOnClickListener(this);
    	  btn_group.setOnClickListener(this);
    	  btn_weather.setOnClickListener(this);
    	  btn_navigation.setOnClickListener(this);
//    	  btn_setting.setOnClickListener(this);
//    	  btn_profile.setOnClickListener(this);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.contact:
			Intent contacts_intent=new Intent(Activity_Main_Fragment.context,Activity_Contacts.class);
			startActivity(contacts_intent);
			break;
		case R.id.party:
			Intent events_intent =new Intent(Activity_Main_Fragment.context,Activity_Party.class);
			startActivity(events_intent);
			break;
		case R.id.news:
			Intent news_intent =new Intent(Activity_Main_Fragment.context,Activity_News.class);
			startActivity(news_intent);
			break;
		case R.id.maps:
			Intent maps_intent=new Intent(Activity_Main_Fragment.context,Activity_MapSchool.class);
			startActivity(maps_intent);
			break;
		case R.id.calender:
			Intent calender_intent=new Intent(Activity_Main_Fragment.context,Activity_Calender.class);
			startActivity(calender_intent);
			break;
		case R.id.deals:
			Intent deal_intent=new Intent(Activity_Main_Fragment.context,Activity_Deals.class);
			startActivity(deal_intent);
			break;
		case R.id.geek:
			Intent group_intent=new Intent(Activity_Main_Fragment.context,Activity_Greek.class);
			startActivity(group_intent);
			break;
		case R.id.weather:
			Intent weather_intent =new Intent(Activity_Main_Fragment.context,Activity_Weather.class);
			startActivity(weather_intent);
			break;
		case R.id.navigation:
//			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<lat>,<long>?"));
//			Intent intent_maps = new Intent(android.content.Intent.ACTION_VIEW,  Uri.parse(url));
//			startActivity(intent);
//			Uri uri = Uri.parse("http://maps.google.com/maps?&saddr=" + mCurrentLatitude+","+mCurrentLongitude+"&daddr="+ mLatitude +"," +mLongitude);
//      Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//      startActivity(intent);
//			String url = "http://maps.google.com/maps?saddr=some+address&daddr=another+Address";
			Intent intent_maps = new Intent(android.content.Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?"));
			intent_maps.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
			startActivity(intent_maps); 
//			startActivity(getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps"));
			break;
//		case R.id.settingBtn:
//			Intent intent_setting =new Intent(Activity_Main_Fragment.context,Activity_Setting.class);
//			startActivity(intent_setting);
//			break;
//		case R.id.profileBtn:
//			Intent intent_profile =new Intent(Activity_Main_Fragment.context,Activity_Profile.class);
//			startActivity(intent_profile);
//			break;

		default:
			break;
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
	      /**
	       * Inflate the layout for this fragment
	       */
	      View v =  inflater.inflate(R.layout.activity_homes, container, false);
	      Registerwidgets(v);
	      setListner();
	      UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
	      if (getActivity() instanceof Activity_Main_Fragment) {
	    	  Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
			  ra.setFooter(rl);
			  ra.setHeader(true,true,"Welcome " + userInfo.first_name,rl);
		  }

	      return v;
	   }
	
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	   super.onActivityResult(requestCode, resultCode, data);
//	   if (resultCode == RESULT_OK && requestCode == 1) {
//	      setResult(RESULT_OK);
//	      finish();
//	   }
//	}	
}

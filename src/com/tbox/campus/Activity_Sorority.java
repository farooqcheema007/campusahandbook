package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import com.tbox.imagedownload.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Sorority extends Activity_Base{
	ArrayList<Greek> 				grouplist;
	JSONArray 						jArray;
	ProgressBar 					pb_sorority;
	static ArrayList<JSONObject> 	json_array_list;
	ListView 						lv;
	private void Registerwidgets(){
		pb_sorority		=	(ProgressBar)findViewById(R.id.progressBarSorority);
		grouplist 		= 	new ArrayList<Greek>();
		json_array_list	= 	new ArrayList<JSONObject>();
		lv				=	(ListView)findViewById(R.id.listviewSorority);
	}
	void sendJsonRequest(){
		sendJsonRequestLoopi();
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(Activity_Sorority.this,Activity_Greek_Details.class);
				Bundle bundle=new Bundle();
				bundle.putString("jsonObject", json_array_list.get(arg2).toString());
				bundle.putString("nameType", "Soro");
				intent.putExtras(bundle) ;
				startActivity(intent);
			}
		});

	}
	void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_sorority, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
							JSONArray jArray1=object.getJSONArray("sorority");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
								
	 							Greek sororityObj = new Greek();
	 							sororityObj.group_name	=	jobject1.getString("group_name");
	 							sororityObj.image_path	=	jobject1.getString("group_image");
	 							sororityObj.phone_no	=	jobject1.getString("phone_number");
	 							sororityObj.email	=	jobject1.getString("website_address");
	 							sororityObj.high_five	=	jobject1.getString("high_five");
	 				            json_array_list.add(jobject1);  	
	 				            grouplist.add(sororityObj);
	 				             
	 						}
		 					pb_sorority.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv.setAdapter(listAdapter);
						}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_sorority.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Sorority.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Greek Life");
		super.setBaseLayout(R.layout.activity_sorority);
		Registerwidgets();
		sendJsonRequest();

	}
	
	class ListAdapter1 extends ArrayAdapter<Greek>{

	 	public ListAdapter1() {
	 		super(Activity_Sorority.this, R.layout.inflate_greek_life_details,grouplist);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_greek_life_details, null);
 		    }
 			TextView tv_name = (TextView)convertView.findViewById(R.id.tvGreekDetailsHeading);
 			TextView tv_number = (TextView)convertView.findViewById(R.id.tvGreekDetailsNumber);
 			TextView tv_email = (TextView)convertView.findViewById(R.id.tvGreekDetailsEmail);
 			TextView tv_high_five = (TextView)convertView.findViewById(R.id.tvGreekViewHighFiveText);
 			ImageView iv_image = (ImageView)convertView.findViewById(R.id.ivGreekDetailsView);
 			
 			tv_name.setText(grouplist.get(position).group_name);
 			tv_email.setText(grouplist.get(position).email);
 			tv_number.setText(grouplist.get(position).phone_no);
 			tv_high_five.setText(grouplist.get(position).high_five);

 			ImageLoader imageLoader	=	new ImageLoader(getApplicationContext());
 			imageLoader.DisplayImage(grouplist.get(position).image_path, iv_image);

 			tv_name.setText(grouplist.get(position).group_name);
 			return convertView;	
		}
	 	

	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}


}

package com.tbox.campus;

import com.example.campus.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SlidingFragment extends Fragment {
	ListView listView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 View v = inflater.inflate(R.layout.list, null);
			 listView = (ListView)v.findViewById(R.id.listListView);
			 RelativeLayout btnSignup = (RelativeLayout) v.findViewById(R.id.searchListRelativeLayout); 
		     btnSignup.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View v) {
		            	switchFragment(4);	
		            }

		        });

			return v;
		
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		String[] birds = {"Home","Profile","My Campus","Setting"};
//		ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(getActivity(), 
//				android.R.layout.simple_list_item_1, android.R.id.text1, birds);
		ListAdapter1 listAdapter = new ListAdapter1(birds);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				
				// TODO Auto-generated method stub
				arg1.setBackgroundColor(0xFF161616);
				for ( int i=0; i < listView.getChildCount(); i++) {
					if(i!=arg2){
						listView.getChildAt(i).setBackgroundColor(0xFF212121);
					}
//					   listView.setItemChecked(i, true);
				}
				switchFragment(arg2);
			}
		});
		
	}
//	@Override
//	public void onListItemClick(ListView lv, View v, int position, long id) {
////		Fragment newContent = new BirdGridFragment(position);
////		if (newContent != null)
//			switchFragment(position);
//	}
	
	// the meat of switching the above fragment
	private void switchFragment(int position) {
//		if (getActivity() == null)
//			return;

		if (getActivity() instanceof Activity_Main_Fragment) {
			Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
			ra.switchContent(position);
		}
	}
	
	
	class ListAdapter1 extends ArrayAdapter<String>{
		private String[] names;
	 	public ListAdapter1(String[] names) {
	 		super(Activity_Main_Fragment.context, R.layout.inflate_listview_textview,names);
	 		// TODO Auto-generated constructor stub
	 		this.names = names;
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)Activity_Main_Fragment.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_sliding_menu, null);
 		    }
			convertView.setBackgroundColor(0xFF212121);
 			TextView tv_title = (TextView)convertView.findViewById(R.id.textviewInflateList);
 			tv_title.setText(names[position]);
 			tv_title.setTextColor(Color.WHITE);
// 			tv_title.setTextSize();
 			return convertView;	
		}
	 	

	}
	



}

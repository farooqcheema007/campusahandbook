package com.tbox.campus;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

public class Utils {
	static JSONArray jArray = null;
	public static ArrayList<String> nameOfEvent = new ArrayList<String>();
	public static ArrayList<String> startDates = new ArrayList<String>();
	public static ArrayList<String> endDates = new ArrayList<String>();
	public static ArrayList<String> descriptions = new ArrayList<String>();
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    public static void showAlertDialog(String message,Context context)
	{
		new AlertDialog.Builder(context)
		.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		})
		.show();
	}
    public static Bitmap decodeUri(Uri selectedImage,Context context) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
        		context.getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                context.getContentResolver().openInputStream(selectedImage), null, o2);
    }
	public static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}
	public static boolean isDataValid(String email, String password,Context context) {
		if(email.length()==0){
			showAlertDialog("your email is empty.....!",context);
			return false;
		}else if(password.length()==0){
			showAlertDialog("your password is empty.....!",context);
			return false;
		}else{	
			if(!isValidEmail(email)){
				showAlertDialog("your email id is not correct...!",context);
				return false;
			}else{
				return true;
			}
		}
	}
	static public int detectSIMState(Context context){
		TelephonyManager telephoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int simState = telephoneMgr.getSimState();
		if(simState == TelephonyManager.SIM_STATE_ABSENT)
			return 1;
		else 
			return 0;
		
	}
//	static public ArrayList<SearchData> ResultForSeacrh(String searchString,final Context context){
//		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
//		final List<NameValuePair> userInfo = new ArrayList<NameValuePair>();	
//		userInfo.add(new BasicNameValuePair("search_string",searchString));
//		userInfo.add(new BasicNameValuePair("api_key",settings.getString("key", null).toString()));
//		final ArrayList<SearchData> searchlist = new ArrayList<SearchData>();
////		JSONArray jArray = null;
//		
//		Thread myThread = new Thread(new Runnable(){
//	          @Override
//	    	   public void run() {
//	        	  jArray = JsonClass.makeHttpRequest(Constant.addess_url_base+Constant.address_search
//	        			  , "GET", userInfo);
//	        	  ((Activity)context).runOnUiThread(new Runnable(){
//	        		  @Override
//	        		  public void run() {
//	        			  try{
//							JSONObject jobject = jArray.getJSONObject(0);
//							int status=jobject.getInt("status");
//							if(status==1){
//								JSONArray jArray1=jobject.getJSONArray("coupons");
//								for(int i=0;i<jArray1.length();i++){
//									JSONObject jobject1 =jArray1.getJSONObject(0);
//									SearchData searchDataObj = new SearchData();
//									searchDataObj.kCoupen_Id= jobject1.getString("coupon_id");
//									searchDataObj.kPlace_At = jobject1.getString("place_at");
//									searchDataObj.kCoupon_Title = jobject1.getString("coupon_title");
//									searchDataObj.kCoupon_Image = jobject1.getString("coupon_image");
//									searchDataObj.kDescription = jobject1.getString("description");
//									searchDataObj.kQR_code_image = jobject1.getString("QR_code_image");
//									searchDataObj.kCoupons_Company_Website = jobject1.getString("coupons_company_website");
//									searchDataObj.kCompany_Website = jobject1.getString("company_website");
//									searchDataObj.kCompany_Phone_Number = jobject1.getString("company_phone_number");
//									searchDataObj.kPlace_At = jobject1.getString("high_five");
//									searchlist.add(searchDataObj);
//								}
//								
//							}
//					    }catch (JSONException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//			    	  }
//	        	  });
//	          }
//		});
//		
//		myThread.start();
//		
//		return searchlist;
//	}
	public static ArrayList<String> readCalendarEvent(Context context) {
		Cursor cursor = context.getContentResolver()
				.query(Uri.parse("content://com.android.calendar/events"),
						new String[] { "calendar_id", "title", "description",
								"dtstart", "dtend", "eventLocation" }, null,
						null, null);
		cursor.moveToFirst();
		// fetching calendars name
		String CNames[] = new String[cursor.getCount()];

		// fetching calendars id
		nameOfEvent.clear();
		startDates.clear();
		endDates.clear();
		descriptions.clear();
		for (int i = 0; i < CNames.length; i++) {

			nameOfEvent.add(cursor.getString(1));
			startDates.add(getDate(Long.parseLong(cursor.getString(3))));
			endDates.add(getDate(Long.parseLong(cursor.getString(4))));
			descriptions.add(cursor.getString(2));
			CNames[i] = cursor.getString(1);
			cursor.moveToNext();

		}
		return nameOfEvent;
	}

	public static String getDate(long milliSeconds) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}




}
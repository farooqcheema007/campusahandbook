package com.tbox.campus;

import com.example.campus.R;
import com.tbox.imagedownload.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Activity_Deal_Image extends Activity{
	TouchImageView iv;
	String strName;
//	Matrix matrix = new Matrix(); 
//	Matrix savedMatrix = new Matrix(); 
//	PointF startPoint = new PointF(); 
//	PointF midPoint = new PointF(); 
//	float oldDist = 1f; 
//	static final int NONE = 0; 
//	static final int DRAG = 1; 
//	static final int ZOOM = 2; 
//	int mode = NONE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_deal_image_detail);
		iv = (TouchImageView)findViewById(R.id.imgDealImage);
		Bundle extras = getIntent().getExtras(); 
		strName = extras.getString("imagePath");//
		ImageLoader iv_loader_obj	=	new ImageLoader(Activity_Deal_Image.this);
		iv_loader_obj.DisplayImage(strName, iv);
		iv.setMaxZoom(4f);
		
//		iv.setOnTouchListener(new View.OnTouchListener() {
//
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				
//				
//				ImageView view = (ImageView) v; 
////				System.out.println("matrix=" + savedMatrix.toString()); 
//				switch (event.getAction() & MotionEvent.ACTION_MASK) { 
//					case MotionEvent.ACTION_DOWN:
//						savedMatrix.set(matrix);
//					    startPoint.set(event.getX(), event.getY());
//					    mode =DRAG;
//					    break;
//					case MotionEvent.ACTION_POINTER_DOWN: 
//						oldDist = spacing(event); 
//						if (oldDist > 10f) { 
//							savedMatrix.set(matrix); 
//							midPoint(midPoint, event); 
//							mode = ZOOM; 
//							} 
//						break; 
//					case MotionEvent.ACTION_UP: 
//					case MotionEvent.ACTION_POINTER_UP: 
//						mode = NONE; 
//						break;
//					case MotionEvent.ACTION_MOVE: 
//						if (mode == DRAG) { 
//							matrix.set(savedMatrix); 
//							matrix.postTranslate(event.getX() - startPoint.x, event.getY() - startPoint.y); 
//							} 
//						else if (mode == ZOOM) { 
//							float newDist = spacing(event); 
//							if (newDist > 10f) { 
//								matrix.set(savedMatrix); 
//								float scale = newDist / oldDist; 
//								matrix.postScale(scale, scale, midPoint.x, midPoint.y); 
//								} 
//						}
//						break;
//
//				}
//
//				view.setImageMatrix(matrix);
//
//			    return true;
//			} 
//			@SuppressLint("FloatMath") 
//			private float spacing(MotionEvent event) { 
//				float x = event.getX(0) - event.getX(1); 
//				float y = event.getY(0) - event.getY(1); 
//				return FloatMath.sqrt(x * x + y * y); 
//			} 
//			private void midPoint(PointF point, MotionEvent event) { 
//				float x = event.getX(0) + event.getX(1); 
//				float y = event.getY(0) + event.getY(1); 
//				point.set(x / 2, y / 2); 
//			}
//		 });
//		
//		
		
	}
	
}

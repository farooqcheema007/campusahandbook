package com.tbox.campus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Login extends Activity_Base implements OnClickListener {
	Button 		btn_login,btn_signUp;
	EditText 	et_email,et_password;
	ProgressBar pb_progressbar_login;
	JSONArray 	jArray;
	int 		status;
	TextView 	tv_link;
	static Activity_Base activity_base;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		if(settings.getString("key", null)!=null){
			if(settings.getString("state", null)==null&&settings.getString("schoolname", null)==null){
				Intent intent_main_fragment =new Intent(Activity_Login.this,Activity_Main_Fragment.class);
				intent_main_fragment.putExtra("key", 1);
				startActivity(intent_main_fragment);
				finish();	
			}else{
				Intent intent_deal_day =new Intent(Activity_Login.this,Activity_Deals_Of_Week.class);
				startActivity(intent_deal_day);
				finish();
			}
		}
		activity_base=this;
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.setHeader(false,false,"");
		super.setBaseLayout(R.layout.activity_login);
		registerWidgets();
		setListener();
		setHyperLink();
	}
	private void registerWidgets(){
		  btn_login		=	(Button)findViewById(R.id.loginBtn);
		  et_email		=	(EditText)findViewById(R.id.lemail);
		  et_password	=	(EditText)findViewById(R.id.lpassword);
		  btn_signUp	=	(Button)findViewById(R.id.signupFromlogin);
		  pb_progressbar_login = (ProgressBar)findViewById(R.id.progressBarLogin);
		  tv_link 		= (TextView)findViewById(R.id.loginTextClick);
		  
	}
	private void setListener(){
		btn_login.setOnClickListener(this);
	    btn_signUp.setOnClickListener(this);
	    pb_progressbar_login.setVisibility(View.INVISIBLE);
	}
	private void setHyperLink(){
		// setting hyperLink in TextView 
	      tv_link.setText(Html.fromHtml(
		            " <font color='black'><u><a href=\"http://www.google.com\">Terms of Services</a></u></font>" +
		            "  and  " +
		            "<font color='black'><u><a href=\"http://www.google.com\">Privacy Policy</a></u></font>"));
	      tv_link.setMovementMethod(LinkMovementMethod.getInstance());

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.loginBtn:
			if(Utils.isDataValid(et_email.getText().toString(),et_password.getText().toString(),Activity_Login.this)){
				pb_progressbar_login.setVisibility(View.VISIBLE);
				sendRequestJsonRegistration();
			}
		break;	
		case R.id.signupFromlogin:
			Intent intent_sign = new Intent(Activity_Login.this,Activity_Register.class);
			startActivity(intent_sign);
		default:
			break;
		}
	}	
	private void sendRequestJsonRegistration(){
		JSONObject json = new JSONObject();
	    try {
			json.put("email_id",et_email.getText().toString());
			json.put("password",et_password.getText().toString());
	    }catch (JSONException e) {
			// TODO Auto-generated catch block
	    	e.printStackTrace();
	    }
	    RequestParams params = new RequestParams();
	    params.put("data", json.toString());

    	AsyncHttpClient client = new AsyncHttpClient();
    	client.post(Constant.addess_url_base+Constant.address_login, params , new JsonHttpResponseHandler(){  

            @Override
            public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					if(status == 1){            			
						JSONObject userData;
						try {
							userData = object.getJSONObject("user");
							UserInfo userInfo 	= 	UserInfo.getInstance(Activity_Login.this);
		            		userInfo.api_key 	= 	userData.getString("api_key");
		            		userInfo.first_name = 	userData.getString("first_name");
		            		userInfo.last_name 	= 	userData.getString("last_name");
		            		userInfo.email_id 	= 	userData.getString("email_id");
		            		userInfo.gender   	= 	userData.getString("gender");
		            		userInfo.profile_image_path = userData.getString("profile_image");
		            		userInfo.noti_email =   Integer.parseInt(userData.getString("email_notification"));
		            		userInfo.noti_push_woosh = Integer.parseInt(userData.getString("push_notification"));
		            		//save user data to share preferences
		            		userInfo.saveUserInfo(Activity_Login.this);
		            		
		            		pb_progressbar_login.setVisibility(View.INVISIBLE);
		            		Intent intent_main_fragment=new Intent(Activity_Login.this,Activity_Main_Fragment.class);
		            		intent_main_fragment.putExtra("key", 1);
		 		        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 		    		SharedPreferences.Editor editor = settings.edit();
		 		    		editor.putString("key",userData.getString("api_key")).commit();
		 		    		startActivity(intent_main_fragment);
		 		    		finish();

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
           	 

            }
            @Override
            protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_progressbar_login.setVisibility(View.INVISIBLE);
				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Login.this);
					}
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

            }
          });

	}
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	   super.onActivityResult(requestCode, resultCode, data);
//	   if (resultCode == RESULT_OK && requestCode == 1) {
//	      setResult(RESULT_OK);
//	      finish();
//	   }
//	}

	
}

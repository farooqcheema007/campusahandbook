

package com.tbox.campus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.campus.R;
import com.facebook.android.AsyncFacebookRunner;
import com.tbox.campus.TwitterApp.TwDialogListener;

@SuppressLint("HandlerLeak")
public class TweetPost extends Activity {
	private TwitterApp mTwitter;
	private ToggleButton mTwitterBtn;
	private String username = "";
	private boolean postToTwitter = true;
	
	private static final String twitter_consumer_key = "m1lV7zrCjAUGxb97keXYUg";
	private static final String twitter_secret_key = "C9nIXsNo3lLTskkJoNqI2Yn2b8ThYmwbNxVrDCBoVzQ";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tweet);
		
		Button postBtn 				= (Button) findViewById(R.id.tbutton1);
		final EditText reviewEdit   = (EditText) findViewById(R.id.revieew);
		
		postBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String review = reviewEdit.getText().toString();
				
				if (review.equals("")) return;
				
				postReview(review);
				
				if (postToTwitter) postToTwitter(review);
			}
		});

		mTwitter = new TwitterApp(this, twitter_consumer_key,twitter_secret_key);
		
		mTwitter.setListener(mTwLoginDialogListener);

		mTwitterBtn	= (ToggleButton) findViewById(R.id.twiterNews);

		mTwitterBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mTwitter.hasAccessToken()) {
					postToTwitter = mTwitterBtn.isChecked();
				} else {
					mTwitterBtn.setChecked(false);
					mTwitter.authorize();
				}
			}
		});
		
		if (mTwitter.hasAccessToken()) {
			username 	= mTwitter.getUsername();
			username	= (username.equals("")) ? "No Name" : username;
			
			
		}
	}
	
	private void postReview(String review) {
		//post to server
		
		Toast.makeText(this, "Review posted", Toast.LENGTH_SHORT).show();
	}
	
	private void postToTwitter(final String review) {
		new Thread() {
			@Override
			public void run() {
				int what = 0;
				
				try {
					mTwitter.updateStatus(review);
				} catch (Exception e) {
					what = 1;
				}
				
				mHandler.sendMessage(mHandler.obtainMessage(what));
			}
		}.start();
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String text = (msg.what == 0) ? "Posted to Twitter" : "Post to Twitter failed";
			
			Toast.makeText(TweetPost.this, text, Toast.LENGTH_SHORT).show();
		}
	};
	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {
		@Override
		public void onComplete(String value) {
			username 	= mTwitter.getUsername();
			username	= (username.equals("")) ? "No Name" : username;
		
			
			mTwitterBtn.setChecked(true);
			
			postToTwitter = true;
			
			Toast.makeText(TweetPost.this, "Connected to Twitter as " + username, Toast.LENGTH_LONG).show();
		}
		
		@Override
		public void onError(String value) {
			mTwitterBtn.setChecked(false);
			
			Toast.makeText(TweetPost.this, "Twitter connection failed", Toast.LENGTH_LONG).show();
		}
	};
}
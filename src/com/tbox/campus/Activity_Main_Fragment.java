package com.tbox.campus;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.arellomobile.android.push.PushManager;
import com.example.campus.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

@SuppressLint("NewApi")
public class Activity_Main_Fragment extends SlidingFragmentActivity{
	@SuppressLint("NewApi")
	FragmentManager fragmentManager = getSupportFragmentManager();
	static Context context;	
	View childFooter , childHeader , childHeadingBar;
//	View childFooter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_fragment);
		context = this;
		
		
		// check if the content frame contains the menu frame
		if (findViewById(R.id.menu_frame) == null) {
			setBehindContentView(R.layout.slidingmenu);
			getSlidingMenu().setSlidingEnabled(true);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		} else {
			// add a dummy view
			View v = new View(this);
			setBehindContentView(v);
			getSlidingMenu().setSlidingEnabled(false);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		}
//		Activity_Home lm_fragment = new Activity_Home();
//		getSupportFragmentManager().beginTransaction().add(android.R.id.content, lm_fragment);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SlidingFragment())
		.commit();

		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setMode(SlidingMenu.LEFT);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
//		sm.setBehindScrollScale(0.35f);
		sm.setFadeDegree(0.35f);
//		sm.setMenu(R.layout.slidingmenu);

		Intent mIntent = getIntent();
		int intValue = mIntent.getIntExtra("key", 0); 
		if(intValue==1){
		    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		    Activity_States ls_fragment = new Activity_States();
		    ls_fragment.valueButton = false;
		    fragmentTransaction.replace(android.R.id.content, ls_fragment);
		    fragmentTransaction.commit();
		}else if(intValue==0){
		    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		    Activity_Home lm_fragment = new Activity_Home();
		    fragmentTransaction.replace(android.R.id.content, lm_fragment);
		    fragmentTransaction.commit();
		}
	      
	}
//	void mainMenu(){
//		getSupportFragmentManager()
//		.beginTransaction()
//		.replace(R.id.menu_frame, new SlidingFragment())
//		.commit();
//
//	}
	void setFooter(RelativeLayout rl){
		
		LayoutInflater flater = LayoutInflater.from(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        childFooter = flater.inflate(R.layout.footer, null);
        childFooter.setLayoutParams(params);
        childFooter.setId(2001);
        rl.addView(childFooter);

	}
	void setHeader(boolean activityStatusBtn,boolean mainHeadingStatus,String headingText , RelativeLayout rl){
		LayoutInflater flater = LayoutInflater.from(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        childHeader = flater.inflate(R.layout.header,rl, false);
        childHeader.setLayoutParams(params);
        childHeader.setId(2000);
		TextView tv_date = (TextView)childHeader.findViewById(R.id.headerDate);
		Calendar c = Calendar.getInstance();
		String strDate = c.getTime().toString();
		
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM");
		String month_name = month_date.format(cal.getTime());
		
		SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
		String goal = outFormat.format(cal.getTime());
		
		String[] arrDate = strDate.split(" ");
		tv_date.setText(goal+"  "+month_name+" "+arrDate[2]+" , "+arrDate[5]);
		tv_date.setTextColor(Color.WHITE);
		Button btn_header = (Button)childHeader.findViewById(R.id.headerBtn);
//		btn_header.setOnClickListener(this);
		if (activityStatusBtn)
			btn_header.setVisibility(View.VISIBLE);
		else 
			btn_header.setVisibility(View.INVISIBLE);
		
		rl.addView(childHeader);
		
		int dpValue = 35; // margin in dips
		float d = this.getResources().getDisplayMetrics().density;
		int margin = (int)(dpValue * d); // margin in pixels
		
		LayoutInflater inflaterLayout = LayoutInflater.from(this);
		RelativeLayout.LayoutParams paramsHeading = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		paramsHeading .addRule(RelativeLayout.BELOW,childHeader.getId());
		paramsHeading .setMargins(0, margin, 0, 0);
        childHeadingBar = inflaterLayout.inflate(R.layout.main_heading, null);
        childHeadingBar.setLayoutParams(paramsHeading);

        childHeadingBar.setId(2003);
		
		ImageView ivHeading = (ImageView)childHeadingBar.findViewById(R.id.mainHeading);
		TextView mainHeadingText = (TextView)childHeadingBar.findViewById(R.id.mainHeadingText);
		if (mainHeadingStatus){
			ivHeading.setVisibility(View.VISIBLE);
			mainHeadingText.setVisibility(View.VISIBLE);
			mainHeadingText.setText(headingText);
		}else{
			ivHeading.setVisibility(View.INVISIBLE);
			mainHeadingText.setVisibility(View.INVISIBLE);		
		}
		
		rl.addView(childHeadingBar);
		
		btn_header.setOnClickListener(new View.OnClickListener() {
	         @Override
	          public void onClick(View v) {
	        	 getSlidingMenu().toggle();
	           }
	         });
	}
	public void callDealActivity(){
		Intent intent_home=new Intent(Activity_Main_Fragment.this,Activity_Main_Fragment.class);
		startActivity(intent_home);
		finish();
	}
	public void callSearchActivity(){
		Intent intent_search=new Intent(Activity_Main_Fragment.this,Activity_Search.class);
		startActivity(intent_search);
		finish();
	}
	public void switchContent(int position) {
		if(position==0){
			  	toggle();
			  	FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			  	Activity_Home lm_fragment = new Activity_Home();
			  	fragmentTransaction.replace(android.R.id.content, lm_fragment);
			  	fragmentTransaction.commit();
		}else if(position==1){
			 	toggle();
			  	FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			  	Activity_Profile lp_fragment = new Activity_Profile();
			  	fragmentTransaction.replace(android.R.id.content, lp_fragment);
			  	fragmentTransaction.commit();

		}else if(position==2){
		 		toggle();
		 		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		 		Activity_States ls_fragment = new Activity_States();
		 		ls_fragment.valueButton = true;
		 		fragmentTransaction.replace(android.R.id.content, ls_fragment);
		 		fragmentTransaction.commit();
		}else if(position==3){
			  	toggle();
			  	FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			  	Activity_Setting lh_fragment = new Activity_Setting();
			  	fragmentTransaction.replace(android.R.id.content, lh_fragment);
			  	fragmentTransaction.commit();

		}else if(position==4){
				Intent events_search =new Intent(Activity_Main_Fragment.context,Activity_Search.class);
				startActivity(events_search);
		}
		
//		fragmentTransaction.commit();
	}
	public void hidenKeyBoard(){
		   getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		setIntent(intent);
		Activity_Setting fragment = (Activity_Setting) getSupportFragmentManager().findFragmentById(android.R.id.content);
		fragment.checkMessage(intent); 
		setIntent(new Intent());
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		super.onActivityResult(requestCode,requestCode,data); 
	    if(resultCode == RESULT_OK)
	    {
	        if(requestCode == 1)
	        {
	    		Activity_Profile fragment = (Activity_Profile) getSupportFragmentManager().findFragmentById(android.R.id.content);
	    		fragment.onActivityResult(requestCode,resultCode,data); 

	        }
	    }
	}
	
}


package com.tbox.campus;

import com.tbox.imagedownload.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

@SuppressLint("NewApi")
public class Activity_Profile extends Fragment implements OnItemSelectedListener,OnClickListener{
	RelativeLayout 		rl = null;
	TextView 			tv_campus_name;
	Button 				btn_Update_Profile , btn_gallery;
	EditText 			et_fName, et_lName ,et_password,et_email,et_confirm_password;
	RadioButton 		rb_male,rb_female;
	SharedPreferences  	prefs;
	ImageView 			iv_profile_image;
	
	String 				gender="" , picturePath="";
//	String 				ProfileImagePathSd ;//= "/mnt/sdcard/MyDir/ProfileImage.jpg";
	int 				status = 0;
	private 			Uri outputFileUri,selectedImageUri;
	ProgressBar			pb;
	ProgressDialog 		mProgressDialog;
	Spinner 			sp;
	ArrayAdapter<String>st;
	String[]			states;
	private void registerWidgets(View v) {
		// TODO Auto-generated method stub
	
        et_fName 			= (EditText)v.findViewById(R.id.firstName);
        et_lName 			= (EditText)v.findViewById(R.id.lastName);
        et_email			= (EditText)v.findViewById(R.id.emailProfile);
        et_password 		= (EditText)v.findViewById(R.id.passwordProfile);
        et_confirm_password = (EditText)v.findViewById(R.id.confirmProfile);
        rb_male 			= (RadioButton)v.findViewById(R.id.maleProfile);
        rb_female 			= (RadioButton)v.findViewById(R.id.femaleProfile);
        iv_profile_image 	= (ImageView)v.findViewById(R.id.imageviewProfile);
//        imageLoader			= new ImageLoader(Activity_Main_Fragment.context);
        btn_Update_Profile 	= (Button)v.findViewById(R.id.updateProfile);
        tv_campus_name		= (TextView)v.findViewById(R.id.campusProfile);
        pb 					= (ProgressBar)v.findViewById(R.id.progressBarProfile);
        rl					= (RelativeLayout)v.findViewById(R.id.profileRelativeLayout);
        sp 					= (Spinner)v.findViewById(R.id.spinnerProfile);
//        btn_gallery 		= (Button)findViewById(R.id.galleryProfile);
        
        states=new String[4];
        states[0] = "FRESHMAN";
        states[1] = "SOPHMORE";
        states[2] = "JUNIOR";
        states[3] = "SENIOR";
	}
    private void setListner(){
      btn_Update_Profile.setOnClickListener(this);
      rb_male.setOnClickListener(this);
      rb_female.setOnClickListener(this);
      iv_profile_image.setOnClickListener(this);
      et_email.setKeyListener(null);
      sp.setOnItemSelectedListener(this);
   	}   
    private void setSpinnerValue(){
		st=new ArrayAdapter<String>(Activity_Main_Fragment.context, android.R.layout.simple_spinner_item,states);
		st.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp.setSelection(states.length-1);
		
		sp.setAdapter(st);

    }
	private void setValueProfile(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
		UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
		if(!userInfo.profile_spinner.equals("")){
			for(int i=0;i<states.length;i++){
				if(userInfo.profile_spinner.equals(states[i])){
					sp.setSelection(i);
				}
			}
		}
			
		et_fName.setText(userInfo.first_name);
		et_lName.setText(userInfo.last_name);
		et_email.setText(userInfo.email_id);
		tv_campus_name.setText(settings.getString("schoolname", null));
		if(userInfo.gender!=null){
			if(userInfo.gender.equals("0")){
				rb_male.setChecked(true);
			    rb_female.setChecked(false);
			    gender = "0";
			}else if(userInfo.gender.equals("1")){
				rb_female.setChecked(true);
			    rb_male.setChecked(false);
			    gender="1";
			}
		}
		if(!userInfo.profile_image_path.equals("")){
			 new DownloadImage().execute(userInfo.profile_image_path);
//			ImageLoader imageLoader = new ImageLoader(Activity_Main_Fragment.context); 
//			imageLoader.DisplayImage(userInfo.profile_image_path , iv_profile_image);
		}
			
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.updateProfile:
			pb.setVisibility(View.VISIBLE);
			sendRequestJsonRegistration(Constant.addess_url_base+Constant.address_profile_update);
			break;
        case R.id.maleProfile:
			rb_male.setChecked(true);
		    rb_female.setChecked(false);
            gender="0";
			break;
         
		case R.id.femaleProfile:
			rb_female.setChecked(true);
		    rb_male.setChecked(false);
		    gender="1";
			break;
		case R.id.imageviewProfile:
			openImageIntent();
			break;
		default:
			break;
		}
			
		
	}
	public boolean sendRequestJsonRegistration(String url){
		
	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
	JSONObject json = new JSONObject();
    try {
    	if(!et_password.getText().toString().equals("")){
    		if(et_password.getText().toString()!=et_confirm_password.getText().toString())
    			json.put("password",et_password.getText().toString());
    		else
    			Utils.showAlertDialog("password does not match..!",Activity_Main_Fragment.context);
    	}
		json.put("first_name",et_fName.getText().toString());
		json.put("last_name",et_lName.getText().toString());
		json.put("api_key",settings.getString("key",null));
		if(rb_male.isChecked())
			json.put("gender","0");
		else if(rb_female.isChecked())
			json.put("gender","1");
		
	}catch (JSONException e) {
			// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	RequestParams params = new RequestParams();
    params.put("data", json.toString());
	AsyncHttpClient client = new AsyncHttpClient();
	client.post(url, params , new JsonHttpResponseHandler(){  

         @Override
         public void onSuccess(final JSONObject object){
					try {
						status = object.getInt("status");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(status == 1){            			
					UserInfo userInfo= UserInfo.getInstance(Activity_Main_Fragment.context);
					userInfo.first_name = et_fName.getText().toString();
					userInfo.last_name = et_lName.getText().toString();
					userInfo.password = et_password.getText().toString();
					userInfo.gender = gender;
					userInfo.profile_spinner = sp.getSelectedItem().toString();
					
					userInfo.saveUserInfo(Activity_Main_Fragment.context);
					if(!picturePath.equals("")){
						SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
						RequestParams paramsImage = new RequestParams();
//						JSONObject jsonObject = new JSONObject();
//					    try {
//					    	jsonObject.put("api_key",settings.getString("key", null).toString());
//							
//						}catch (JSONException e) {
//								// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
						File myFile = new File(picturePath);
					    try {
					    	paramsImage.put("image", myFile);
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
//					    paramsImage.put("data", jsonObject.toString());
				        //send get request for New Details
				    	AsyncHttpClient client = new AsyncHttpClient();
				    	client.setTimeout(60000);
				    	client.post(Constant.addess_url_base + Constant.address_image_uploading+settings.getString("key", null).toString(), paramsImage , new JsonHttpResponseHandler(){  

				             @Override
				             public void onSuccess(final JSONObject object){
				            	int status = 0;
								try {
									status = object.getInt("status");
									 if(status ==1){ 
										 UserInfo userInfo= UserInfo.getInstance(Activity_Main_Fragment.context);
										 userInfo.profile_image_path = object.getString("image_path");
										 userInfo.saveUserInfo(Activity_Main_Fragment.context);

					 				}
						 			pb.setVisibility(View.INVISIBLE);
						 			showAlertDialog("Data updated....!");
								
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
				            	
				             }
				             @Override
				             public void onSuccess(int statusCode, JSONObject response) {
				            	Log.d("testing", response.toString());
				            	int status = 0;
									try {
										status = response.getInt("status");
										 if(status ==1){ 
											 UserInfo userInfo= UserInfo.getInstance(Activity_Main_Fragment.context);
											 userInfo.profile_image_path = response.getString("profile_image");
											 userInfo.saveUserInfo(Activity_Main_Fragment.context);
											 
						 				}
										 pb.setVisibility(View.INVISIBLE);
										 showAlertDialog("Data updated....!");
									
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
				             }
				             @Override
				             public void onFailure(Throwable e, JSONObject errorResponse) {
			 	             Log.d("testing", errorResponse.toString());
				             }
				             @Override
				             protected void handleFailureMessage(Throwable e, String responseBody) {
				            	pb.setVisibility(View.INVISIBLE);
				 				try{
									JSONObject jobject =  new JSONObject(responseBody);
									int status = jobject.getInt("status");
									if(status ==0)
										Utils.showAlertDialog(jobject.getString("error_message"),Activity_Main_Fragment.context);
									
								} catch (Exception ex) {
									// TODO Auto-generated catch block
									ex.printStackTrace();
								}

				             }
				    	});

					}else{
						pb.setVisibility(View.INVISIBLE);
						showAlertDialog("Data updated....!");
					}
//					login();	
//						new Image_Upload(Activity_Profile.this,,"http://thecampushandbook.thestudentauthority.com/campus_hb/api/update_profile_image/"+userInfo.api_key,"image","image/jpeg").execute();
						
//					login();
				}else
					Utils.showAlertDialog("Your Information is not updated....!",Activity_Main_Fragment.context);
        	 

         }
       });

	return true;
	}
	private void openImageIntent(){

		// Determine Uri of camera image to save.
		final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
		root.mkdirs();
		final String fname = "ProfileImage.jpg";
		final File sdImageMainDirectory = new File(root, fname);
		picturePath = sdImageMainDirectory.toString();
		outputFileUri = Uri.fromFile(sdImageMainDirectory);

		    // Camera.
		    final List<Intent> cameraIntents = new ArrayList<Intent>();
		    final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		    final PackageManager packageManager = Activity_Main_Fragment.context.getPackageManager();
		    final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
		    for(ResolveInfo res : listCam) {
		        final String packageName = res.activityInfo.packageName;
		        final Intent intent = new Intent(captureIntent);
		        intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
		        intent.setPackage(packageName);
		    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
		        cameraIntents.add(intent);
		    }

		    // Filesystem.
		    final Intent galleryIntent = new Intent();
		    galleryIntent.setType("image/*");
		    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

		    // Chooser of filesystem options.
		    final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

		    // Add the camera options.
		    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

		    getActivity().startActivityForResult(chooserIntent, 1);
		}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
			boolean flagStatus;
	        if(requestCode == 1){
	            final boolean isCamera;
	            if(data == null)
	            {
	                isCamera = true;
	            }
	            else
	            {
	                final String action = data.getAction();
	                if(action == null)
	                {
	                    isCamera = false;
	                }
	                else
	                {
	                    isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	                }
	            }
	            
	            if(isCamera){
	                selectedImageUri = outputFileUri;
	                flagStatus = true;
	            }else{
	                selectedImageUri = data == null ? null : data.getData();
	                flagStatus = false;
	            }
	            try {
					iv_profile_image.setImageBitmap(Utils.decodeUri(selectedImageUri,Activity_Main_Fragment.context));
					if(flagStatus)
						saveBitmap(selectedImageUri , true);
					else
						saveBitmap(selectedImageUri , false);

//						saveBitmap(selectedImageUri);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
//		    }
		}
		private void saveBitmap(Uri uri , boolean valueIntent)
		{
			ContentResolver cr = Activity_Main_Fragment.context.getContentResolver();
			InputStream is = null;
			try {
				is = cr.openInputStream(uri);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
					e1.printStackTrace();
			}
			Options optionSample = new BitmapFactory.Options();
			optionSample.inSampleSize = 4; // Or 8 for smaller image
			Bitmap bMap = BitmapFactory.decodeStream(is, null, optionSample);
			if(bMap.getHeight()>500&&bMap.getWidth()>500){
				File file = new File(picturePath);
				if(!file.exists())
					file.mkdir();
				Log.i("path", "" + file);
				try {
					FileOutputStream out = new FileOutputStream(file);
					bMap.compress(Bitmap.CompressFormat.JPEG, 100, out);
					out.flush();
					out.close();
		//				picturePath = ProfileImagePathSd;
				} catch (Exception e) {
						e.printStackTrace();
				} 
			}else if(!valueIntent){
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = Activity_Main_Fragment.context.getContentResolver().query(uri,filePathColumn, null, null, null);
		        cursor.moveToFirst();
		        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		        picturePath = cursor.getString(columnIndex);
		        cursor.close();
			}
		}

//	void login(){
//		Intent intent_home =new Intent(Activity_Profile.this,Activity_Home.class);
//		intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
// 		startActivity(intent_home);
// 		finish();
//	}
    private void showAlertDialog(String message)
	{
		new AlertDialog.Builder(Activity_Main_Fragment.context)
		.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
//				   Intent intent_home = new Intent(Activity_Profile.this,Activity_Home.class);
//				   intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				   startActivity(intent_home);
//				   finish();

			}
		})
		.show();
	}
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
	      /**
	       * Inflate the layout for this fragment
	       */
//		 super.onCreateView(,container);
	      View v =  inflater.inflate(R.layout.activity_profile, container, false);
	      registerWidgets(v);
	      setListner();
	      setSpinnerValue();
	      setValueProfile();
//	      UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
	      if (getActivity() instanceof Activity_Main_Fragment) {
	    	  Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
			  ra.setFooter(rl);
			  ra.setHeader(true,false,"",rl);
			  ra.hidenKeyBoard();
		  }
	     
	      return v;
	   }

	 // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Activity_Main_Fragment.context);
            // Set progressdialog title
//            mProgressDialog.setTitle("Download Image Tutorial");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }
 
        @Override
        protected Bitmap doInBackground(String... URL) {
 
            String imageURL = URL[0];
 
            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
 
        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
        	iv_profile_image.setImageBitmap(result);
            // Close progressdialog
            mProgressDialog.dismiss();
        }
    }
//    private void showAlertDialog(String message,Context context)
//	{
//		new AlertDialog.Builder(context)
//		.setMessage(message)
//		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int which){
//				login();
//			}
//		})
//		.show();
//	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}



}

package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.campus.R;
import com.tbox.imagedownload.ImageLoader;

public class BaseAdapterParty extends BaseAdapter implements OnClickListener{
	private Context context;
	private ArrayList<Party> eventList;
	public ImageLoader 	imageLoader;
	
	BaseAdapterParty(ArrayList<Party> eventList,Context context){
		this.context=context;
		this.eventList=eventList;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return eventList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(context.getApplicationContext());
		View gridView;
		if (convertView == null) {
			gridView = new View(context);
			 
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.inflate_party_view, null);
			ImageView btn_add_event=(ImageView)gridView.findViewById(R.id.Inflate_party_Add_Events);
			TextView textView = (TextView) gridView.findViewById(R.id.grid_item_label);
			ImageView imageView = (ImageView) gridView.findViewById(R.id.grid_item_image);
			LayoutParams paramBackSearch = new LayoutParams(Activity_Party.width,Activity_Party.height);
			// set image based on selected text
			gridView.setLayoutParams(paramBackSearch);

			if((eventList.size()-1)==position){
				btn_add_event.setVisibility(View.VISIBLE);
				textView.setVisibility(View.INVISIBLE);
				imageView.setVisibility(View.INVISIBLE);
				
				btn_add_event.setOnClickListener(this);
				
				
			}else{
				btn_add_event.setVisibility(View.INVISIBLE);
				textView.setVisibility(View.VISIBLE);
				imageView.setVisibility(View.VISIBLE);
				textView.setText(eventList.get(position).event_category);
				
				imageLoader.DisplayImage(eventList.get(position).event_category_image, imageView);
			}
		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.Inflate_party_Add_Events:
			Intent news_intent =new Intent(context,Activity_Add_Event.class);
			context.startActivity(news_intent);
			break;

		default:
			break;
		}
	}

}

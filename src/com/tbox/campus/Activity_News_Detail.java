package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_News_Detail extends Activity_Base {
	ArrayList<NewsDetail> 			newslist;
	JSONArray 							jArray;
	ProgressBar 						pb_news_detail;
	EditText 							et_Search;
	ArrayList<SearchData> 				searchListView;
	ListView 							lv_news_deatils;
	String[]							news_name;
	TextView							tv_news_subheading;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"News");
		super.setBaseLayout(R.layout.activity_news_detail);
		Registerwidgets();
		sendJsonRequest();
		
		
	}
	private void Registerwidgets(){
		newslist 		= 	new ArrayList<NewsDetail>();
		pb_news_detail 	= 	(ProgressBar)findViewById(R.id.progressBarNewDetail);
		searchListView	=	new ArrayList<SearchData>();
		lv_news_deatils	=	(ListView)findViewById(R.id.listViewNewsDetails);
		tv_news_subheading	=	(TextView)findViewById(R.id.mainHeadingTextNews);
		
		//setting sub News Heading 
		Bundle bundle=getIntent().getExtras();
		mainSubHeadingType(bundle.getString("category_type"));
		
	}
	void sendJsonRequest(){
		sendJsonRequestLoopi();
//		ListView lv=(ListView) findViewById(android.R.id.list);
		lv_news_deatils.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(Activity_News_Detail.this,Activity_News_Detail_Read.class);
				Bundle bundle=new Bundle();
				bundle.putString("news_description", newslist.get(arg2).news_description);
				bundle.putString("news_image", newslist.get(arg2).news_image);
				bundle.putString("news_title", newslist.get(arg2).news_title);
				intent.putExtras(bundle) ;
				startActivity(intent);	
			}
		});

		
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	   if (keyCode == KeyEvent.KEYCODE_BACK) {
	      setResult(RESULT_OK);
	   }
	   return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	   super.onActivityResult(requestCode, resultCode, data);
	   if (resultCode == RESULT_OK && requestCode == 1) {
	      setResult(RESULT_OK);
	      finish();
	   }
	}

	void sendJsonRequestLoopi(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		Bundle bundle=getIntent().getExtras();
		String id=""+settings.getInt("schoolid", 0);
		params.put("category_id",bundle.getString("category_id"));
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_news_detail, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
		 					JSONArray jArray1=object.getJSONArray("categories");
		 					for(int i=0;i<jArray1.length();i++){
		 						JSONObject	jobject1 = jArray1.getJSONObject(i);
		 						NewsDetail news_detail_obj = 	new NewsDetail();
		 						news_detail_obj.news_id	= 	jobject1.getString("news_id");
		 						news_detail_obj.news_title 	=	jobject1.getString("news_title");
    							news_detail_obj.news_description = jobject1.getString("news_description");
    							news_detail_obj.date = jobject1.getString("date");
    							news_detail_obj.news_image	=	jobject1.getString("news_image");
		 			             
		 			            newslist.add(news_detail_obj);
		 			             
		 					}
		 					pb_news_detail.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv_news_deatils.setAdapter(listAdapter);

//		 					ListAdapter adapter = new SimpleAdapter(Activity_News_Detail.this,newslist,
//		 							R.layout.contactlist, new String[] { "news_title","date"},new int[]{R.id.category,R.id.newsdate});
//		 					setListAdapter(adapter);
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_news_detail.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
//						UserInfo userInfo= UserInfo.getInstance(context);	
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_News_Detail.this);
//						userInfo.saveUserInfo(context);
					}
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	class ListAdapter1 extends ArrayAdapter<NewsDetail>{

	 	public ListAdapter1() {
	 		super(Activity_News_Detail.this, R.layout.inflate_listview_textview,newslist);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_listview_textview, null);
 		    }
 			TextView tv_title = (TextView)convertView.findViewById(R.id.textviewInflateList);
 			tv_title.setText(newslist.get(position).date+"-"+newslist.get(position).news_title);
 			return convertView;	
		}
	 	

	}
	void mainSubHeadingType(String strName){
		if(strName.contains("New"))
			tv_news_subheading.setText("Campus News");
		if(strName.contains("Exam"))
			tv_news_subheading.setText("Exams");
		if(strName.contains("Sports"))
			tv_news_subheading.setText("Sports And Clubs");
		if(strName.contains("Volenteer"))
			tv_news_subheading.setText("Volunteer");
		if(strName.contains("Employment"))
			tv_news_subheading.setText("Employment");
		if(strName.contains("Clas"))
			tv_news_subheading.setText("Classifieds");

	}

}

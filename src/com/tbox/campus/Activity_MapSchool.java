package com.tbox.campus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_MapSchool extends Activity_Base implements OnItemSelectedListener,OnClickListener{
	String []							images;
	JSONArray 							jArray;
	ProgressBar 						progressBar;
	String[]							st_collage_lan,st_collage_lon,st_collage_name,st_collage_id;
	Spinner 							sp_school_campus;
	TextView 							tv_school_name;
//	String 								lat="",lng="";
	boolean 							flagStatus;
	private void Registerwidgets(){
		progressBar 		= 	(ProgressBar)findViewById(R.id.progressBarMap);
		sp_school_campus 	= 	(Spinner)findViewById(R.id.spinnerMapSchool);
		tv_school_name 		=	(TextView)findViewById(R.id.textSchoolName);
	}
	private void setListner(){
		sp_school_campus.setOnItemSelectedListener(this);
	}
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Maps");
		super.setBaseLayout(R.layout.activity_maps);
		flagStatus = false;
		Registerwidgets();
		sendJsonRequestLoopi();
		
	}
	
	
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		if(flagStatus){
		if(arg2!=0){
	     if(sp_school_campus.getId() == R.id.spinnerMapSchool){
	    	 
        	  if(st_collage_name[arg2].equals(arg0.getItemAtPosition(arg2).toString())){
//        		  lat 		= 	st_collage_lan[arg2-1];
//        		  lng 		=	st_collage_lon[arg2-1];
        		  
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
					Uri.parse("geo:"+ st_collage_lan[arg2-1] +","+st_collage_lon[arg2-1]+"?q="+st_collage_lan[arg2-1]+","+st_collage_lon[arg2-1]));
//							Uri.parse("geo:"+ 31.545050 +","+74.340683+"?q="+31.545050+","+74.340683));
							// the following line should be used if you want use only Google maps
					intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
					startActivity(intent);

        		  
        	  }
	     }
		}
		}
		flagStatus= true;
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}


	void sendJsonRequestLoopi(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		tv_school_name.setText(settings.getString("state", null));
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.addess_map, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
		 					JSONArray jArray1=object.getJSONArray("maps");
		 					st_collage_lan = new String[jArray1.length()];
		 					st_collage_lon = new String[jArray1.length()];
		 					st_collage_name = new String[jArray1.length()+1];
		 					st_collage_id = new String[jArray1.length()];
		 					
		 					SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_MapSchool.this);
		 					st_collage_name[0] = "Select Campus";
		 					for(int i=0;i<jArray1.length();i++){
		 						 JSONObject	jobject1 = jArray1.getJSONObject(i);
		 						 st_collage_lan[i]	=	jobject1.getString("longitude");
		 						 st_collage_lon[i]	=	jobject1.getString("latitude");
		 			             st_collage_name[i+1]	=	jobject1.getString("college_name");
		 			             st_collage_id[i]	=	jobject1.getString("college_id");
		 					}
		 					progressBar.setVisibility(View.INVISIBLE);
		 					ArrayAdapter<String> st=new ArrayAdapter<String>(Activity_MapSchool.this, android.R.layout.simple_spinner_item,st_collage_name);
							st.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							
							sp_school_campus.setAdapter(st);
							setListner();
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	progressBar.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_MapSchool.this);
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}


}

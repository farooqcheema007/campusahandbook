package com.tbox.campus;

import com.tbox.classes.*;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Contacts extends Activity_Base {
String[]							category;
ArrayList<Contacts> 				contactList;
JSONArray 							jArray;
ProgressBar 						progressBar;
EditText 							et_Search;
//ArrayList<SearchData> 				searchListView;
ListView 							lv;
//Contacts							contacts;

	private void Registerwidgets(){
		progressBar = 	(ProgressBar)findViewById(R.id.progressBarContact);
		contactList = 	new ArrayList<Contacts>();
		lv			=	(ListView)findViewById(R.id.listViewContacts);
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Contacts");
		super.setBaseLayout(R.layout.activity_contacts);
		
		Registerwidgets();
		// Send Json Request  
		sendJsonRequest();
		
	}
	private void sendJsonRequest(){
		sendJsonRequestLoopi();
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				;
				if(Utils.detectSIMState(Activity_Contacts.this)==1){
					Utils.showAlertDialog("No SIM Detect...!",Activity_Contacts.this);
				}else{
					 String uri = "tel:" +contactList.get(arg2).phone_number;
					 Intent intent_call = new Intent(Intent.ACTION_CALL);
					 intent_call.setData(Uri.parse(uri));
					 startActivity(intent_call);
				}
			}
		});

	}

	private void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_contacts, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try{
					status = object.getInt("status");
					if(status ==1){ 
							JSONArray jArray1=object.getJSONArray("contacts");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
								 Contacts contactObj = new Contacts();
								 contactObj.contact_id	= 	jobject1.getString("contact_id");
								 contactObj.person_name	= 	jobject1.getString("person_name");
								 contactObj.catagory 	=	jobject1.getString("catagory");
								 contactObj.phone_number=	jobject1.getString("phone_number");
					              
					             contactList.add(contactObj);
							}
							progressBar.setVisibility(View.INVISIBLE);
							ListAdapter1 listAdapter=new ListAdapter1();
							lv.setAdapter(listAdapter);
					}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	progressBar.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Contacts.this);
					}
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
	class ListAdapter1 extends ArrayAdapter<Contacts>{

	 	public ListAdapter1() {
	 		super(Activity_Contacts.this, R.layout.inflate_listview_contacts,contactList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_listview_contacts, null);
 		    }
 			TextView tv_person_name = (TextView)convertView.findViewById(R.id.textviewInflateListContactsName);
 			TextView tv_person_number = (TextView)convertView.findViewById(R.id.textviewInflateListContactsPhone);
 			tv_person_name.setText(contactList.get(position).person_name);
 			tv_person_number.setText(contactList.get(position).phone_number);
 			return convertView;	
		}
	 	

	}


	
}

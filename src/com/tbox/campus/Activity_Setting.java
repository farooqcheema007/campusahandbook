package com.tbox.campus;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

@SuppressLint("NewApi")
public class Activity_Setting extends Fragment implements OnClickListener{
	ImageView iv_pushWoosh , iv_email;
	TextView  tv_link;
	private static final String APP_ID ="E5E2A-63EC9";
	private static final String SENDER_ID = "327178553010";
	boolean flagpPush;
	boolean flagEmail;
	RelativeLayout rl = null;

//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		super.setHeader(false,false,"");
//		super.setBaseLayout(R.layout.activity_setting);
//		Registerwidgets();
//		setListener();
//		setHyperLink();
//	     
//
//	}
	private void Registerwidgets(View v){
		iv_pushWoosh = (ImageView)v.findViewById(R.id.pushSetting);
		iv_email = (ImageView)v.findViewById(R.id.emailSetting);
		tv_link = (TextView)v.findViewById(R.id.settingTextClick);
		rl = (RelativeLayout)v.findViewById(R.id.settingRelativeLayout);
		
	}
	private void setListener(){
		iv_pushWoosh.setOnClickListener(this);
		iv_email.setOnClickListener(this);
	}
	private void setImageDrawableNotifcation(){
		UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
		if(userInfo.noti_push_woosh==0){
			flagpPush = false;
			iv_pushWoosh.setBackgroundResource(R.drawable.notification_off);
		}else{
			registerReceivers();
			 //Create and start push manager
		    PushManager pushManager = new PushManager(Activity_Main_Fragment.context, APP_ID, SENDER_ID);
		    pushManager.onStartup(Activity_Main_Fragment.context);
		 
		    checkMessage(getActivity().getIntent());
		    flagpPush = true;
		    iv_pushWoosh.setBackgroundResource(R.drawable.notification_on);
		}
		if(userInfo.noti_email==0){
			flagEmail = false;
			iv_email.setBackgroundResource(R.drawable.notification_off);
		}else{
			flagEmail = true;
			iv_email.setBackgroundResource(R.drawable.notification_on);
		}
		
	}
	private void setHyperLink(){
	      //Seting Hyperlink in TextView 
	      tv_link.setText(Html.fromHtml(
		            " <font color='black'><u><a href=\"http://www.google.com\">Terms of Services</a></u></font>" +
		            "  and  " +
		            "<font color='black'><u><a href=\"http://www.google.com\">Privacy Policy</a></u></font>"));
	      tv_link.setMovementMethod(LinkMovementMethod.getInstance());

	}

	//Registration receiver
	BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver()
	{
	    @Override
	    public void onRegisterActionReceive(Context context, Intent intent)
	    {
	    		checkMessage(intent);
	    }
	};
	 
	//Push message receiver
	private BroadcastReceiver mReceiver = new BasePushMessageReceiver()
	{
	    @Override
	    protected void onMessageReceive(Intent intent)
	    {
	        //JSON_DATA_KEY contains JSON payload of push notification.
	    		showMessage("push message is " + intent.getExtras().getString(JSON_DATA_KEY));
	    		
	    }
	};
	 
	//Registration of the receivers
	public void registerReceivers()
	{
	    IntentFilter intentFilter = new IntentFilter(Activity_Main_Fragment.context.getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");
	 
	    Activity_Main_Fragment.context.registerReceiver(mReceiver, intentFilter);
	     
	    Activity_Main_Fragment.context.registerReceiver(mBroadcastReceiver, new IntentFilter(Activity_Main_Fragment.context.getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));       
	}
	 
	public void unregisterReceivers()
	{
	    //Unregister receivers on pause
	    try
	    {
	    	Activity_Main_Fragment.context.unregisterReceiver(mReceiver);
	    }
	    catch (Exception e)
	    {
	        // pass.
	    }
	     
	    try
	    {
	    	Activity_Main_Fragment.context.unregisterReceiver(mBroadcastReceiver);
	    }
	    catch (Exception e)
	    {
	        //pass through
	    }
	}
	@Override
	public void onResume()
	{
	    super.onResume();
	     
	    //Re-register receivers on resume
	    	registerReceivers();
	}
	 
	@Override
	public void onPause()
	{
	    super.onPause();
	 
	    //Unregister receivers on pause
	    sendRequestJsonRegistration();
	    unregisterReceivers();
	}
	
	
	
	public void checkMessage(Intent intent)
	{
	    if (null != intent)
	    {
	        if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
	        {
	            showMessage("push message is " + intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT));
	        }
	        else if (intent.hasExtra(PushManager.REGISTER_EVENT))
	        {
	            showMessage("register");
	        }
	        else if (intent.hasExtra(PushManager.UNREGISTER_EVENT))
	        {
	            showMessage("unregister");
	        }
	        else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
	        {
	            showMessage("register error");
	        }
	        else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
	        {
	            showMessage("unregister error");
	        }
	 
	        resetIntentValues();
	    }
	}
	 
	/**
	 * Will check main Activity intent and if it contains any PushWoosh data, will clear it
	 */
	private void resetIntentValues()
	{
	    Intent mainAppIntent = getActivity().getIntent();
	 
	    if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
	    {
	        mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
	    }
	    else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT))
	    {
	        mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
	    }
	    else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT))
	    {
	        mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
	    }
	    else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
	    {
	        mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
	    }
	    else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
	    {
	        mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
	    }
	 
	    getActivity().setIntent(mainAppIntent);
	}
	 
	private void showMessage(String message)
	{
//	    Toast.makeText(Activity_Main_Fragment.context, message, Toast.LENGTH_LONG).show();
	}
//	@Override
//	protected void onNewIntent(Intent intent)
//	{
//	    super.onNewIntent(intent);
//	    setIntent(intent);
//	 
//	    checkMessage(intent);
//	 
//	    setIntent(new Intent());
//	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.pushSetting:
				UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
				if(!flagpPush){
					 userInfo.noti_push_woosh = 1; 
					 registerReceivers();
					 //Create and start push manager
				      PushManager pushManager = new PushManager(Activity_Main_Fragment.context, APP_ID, SENDER_ID);
				      pushManager.onStartup(Activity_Main_Fragment.context);
				 
				      checkMessage(getActivity().getIntent());
				      iv_pushWoosh.setBackgroundResource(R.drawable.notification_on);
				      flagpPush = true;
				}else{
					userInfo.noti_push_woosh = 0;
					PushManager pushManager = new PushManager(Activity_Main_Fragment.context, APP_ID, SENDER_ID);
					pushManager.stopTrackingGeoPushes();
					pushManager.unregister();
					iv_pushWoosh.setBackgroundResource(R.drawable.notification_off);
					flagpPush=false;
				}
				userInfo.saveUserInfo(Activity_Main_Fragment.context);
				break;
			case R.id.emailSetting:
				UserInfo userInfoData = UserInfo.getInstance(Activity_Main_Fragment.context);
				if(!flagEmail){
					userInfoData.noti_email = 1; 
					iv_email.setBackgroundResource(R.drawable.notification_on);
					flagEmail=true;
				}else{
					userInfoData.noti_email = 0;
					iv_email.setBackgroundResource(R.drawable.notification_off);
					flagEmail=false;
				}
				userInfoData.saveUserInfo(Activity_Main_Fragment.context);
				break;

		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
	      /**
	       * Inflate the layout for this fragment
	       */
	      View v =  inflater.inflate(R.layout.activity_setting, container, false);
//	      setHeader(false,false,"");
//			setBaseLayout(R.layout.activity_setting);
			Registerwidgets(v);
			setListener();
			setHyperLink();
			setImageDrawableNotifcation();
	      if (getActivity() instanceof Activity_Main_Fragment) {
	    	  Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
			  ra.setFooter(rl);
			  ra.setHeader(true,false,"",rl);
		  }

	      return v;
	   }
	private void sendRequestJsonRegistration(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
		UserInfo userInfo = UserInfo.getInstance(Activity_Main_Fragment.context);
		JSONObject json = new JSONObject();
	    try {
			json.put("email_notification",String.valueOf(userInfo.noti_email));
			json.put("push_notification",String.valueOf(userInfo.noti_push_woosh));
			json.put("api_key",settings.getString("key", null).toString());
//			json.put("last_name",et_lname.getText().toString());
//			json.put("email_id",et_email.getText().toString());
//			if(rb_male.isChecked())
//				json.put("gender",gender);
//			else if(rb_female.isChecked())
//				json.put("gender",gender);
			
		}catch (JSONException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RequestParams params = new RequestParams();
	    params.put("data", json.toString());

    	AsyncHttpClient client = new AsyncHttpClient();
    	client.post(Constant.addess_url_base+Constant.address_setting_noti, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							int status = object.getInt("status");
//							if(status ==1)
//								Toast.makeText(getActivity(), "hello", Toast.LENGTH_SHORT).show();
								
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

             }
           });
	}


}

package com.tbox.campus;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Party_Details_Read extends Activity_Base{
	ImageView	iv_main,website_btn,have_five_btn,navi_btn,call_btn;
	TextView 	tvPartyTitleText,tvParty1Text,tvParty2Text,tvPartyWebsite,
					tvPartyAddress,tvPartyPhoneNo,tvPartyInstruction,tvPartyHighFive;
	String 		party_company_website,party_company_phone_No,event_id,lat, lon ;
	ProgressBar	progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		super.setHeader(false,true,getIntent().getStringExtra("event_catgory"));
		super.setBaseLayout(R.layout.activity_read_party_details);		

		Registerwidgets();
		setListner();
		tvPartyInstruction.setMovementMethod(new ScrollingMovementMethod());
		
		JSONObject 	jsonObj = null;
		try {
			jsonObj = new JSONObject(getIntent().getStringExtra("jsonObject"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(jsonObj!=null)
			mainMenuSetting(jsonObj);

	}
	private	void Registerwidgets(){
		iv_main = (ImageView)findViewById(R.id.ivPatyDetailsImageSubHeading);
		tvPartyTitleText=(TextView)findViewById(R.id.tvTitlePartyImage);
		tvParty1Text=(TextView)findViewById(R.id.tvParty1text);
		tvParty2Text=(TextView)findViewById(R.id.tvParty2text);
		tvPartyWebsite=(TextView)findViewById(R.id.tvPartyDetailWebsiteText);
		tvPartyPhoneNo=(TextView)findViewById(R.id.tvPartyDetailPhoneText);
		tvPartyAddress=(TextView)findViewById(R.id.tvPartyDetailAddressText);
		tvPartyInstruction=(TextView)findViewById(R.id.tvPartyDetailInstructionText);
		tvPartyHighFive=(TextView)findViewById(R.id.tvCoupenHighFiveDealImage);
		progressBar=(ProgressBar)findViewById(R.id.progressBarReadPartyDetail);
		website_btn=(ImageView)findViewById(R.id.webSitePartyDetail);
		call_btn=(ImageView)findViewById(R.id.callPartyDetail);
		navi_btn=(ImageView)findViewById(R.id.naviPartyDetail);
		have_five_btn=(ImageView)findViewById(R.id.highFivePartyDetail);
		
	}
	private void setListner(){
		navi_btn.setOnClickListener(this);
		have_five_btn.setOnClickListener(this);
		website_btn.setOnClickListener(this);
		call_btn.setOnClickListener(this);
	}


	private void mainMenuSetting(JSONObject jsonOject){
		
		ImageLoader iv_loader_obj	=	new ImageLoader(Activity_Party_Details_Read.this);
		try {
			iv_loader_obj.DisplayImage(jsonOject.getString("event_image"), iv_main);
			tvPartyTitleText.setText(jsonOject.getString("event_name"));
			tvParty1Text.setText("@ "+jsonOject.getString("event_deal1"));
			tvParty2Text.setText(jsonOject.getString("event_deal2"));
			party_company_website="http://"+jsonOject.getString("website");
			party_company_phone_No="tel:" +jsonOject.getString("phone_number");
			tvPartyAddress.setText(jsonOject.getString("event_address"));
			tvPartyInstruction.setText(jsonOject.getString("special_instructions"));
//			tvPartyWebsite.setText(jsonOject.getString("website"));
			tvPartyPhoneNo.setText(jsonOject.getString("phone_number"));

			tvPartyWebsite.setText(Html.fromHtml(
		            " <font color='blue'><u><a href=\""+"http://"+jsonOject.getString("website")+"\">"+jsonOject.getString("website") +"</a></u></font>"));
			tvPartyWebsite.setMovementMethod(LinkMovementMethod.getInstance());

			
			
			tvPartyHighFive.setText(jsonOject.getString("high_five"));
			event_id=jsonOject.getString("event_id");
			lon=jsonOject.getString("longitude");
			lat=jsonOject.getString("latitude");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.callPartyDetail:
				Intent intent_call = new Intent(Intent.ACTION_CALL);
				intent_call.setData(Uri.parse(party_company_phone_No));
				startActivity(intent_call);
				break;
			case R.id.webSitePartyDetail:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(party_company_website));
			    startActivity(browserIntent);
				break;
			case R.id.naviPartyDetail:
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("geo:"+ lat +","+lon+"?q="+lat+","+lon));
//						Uri.parse("geo:"+ 31.545050 +","+74.340683+"?q="+31.545050+","+74.340683));
						// the following line should be used if you want use only Google maps
				intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
				startActivity(intent);

				break;
			case R.id.highFivePartyDetail:
				progressBar.setVisibility(View.VISIBLE);
				highFiveHitsStatus();
//				high_five_message.setVisibility(View.INVISIBLE);
				break;
		}
	}
		void highFiveHitsStatus(){
			RequestParams params = new RequestParams();
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
//			String id=""+settings.getInt("schoolid", 0);
			params.put("event_id",event_id);
			params.put("api_key",settings.getString("key", null).toString());
	        
	        //send get request for New Details
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.get(Constant.addess_url_base+Constant.addres_party_high_five, params , new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
	            	int status = 0;
					try{
						status = object.getInt("status");
						if(status ==1){
							progressBar.setVisibility(View.INVISIBLE);
							if(object.getString("success_message").equals("You already have done high five action."))
								Toast.makeText(getApplicationContext(), "Already done highfive", Toast.LENGTH_SHORT).show();
//							String strValue = object.getInt("high_five");
							tvPartyHighFive.setText(object.getString("high_five"));
							changeList(event_id,object.getString("high_five"));
//							high_five_message.setText(object.getString("success_message"));
						}
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
	            	
	             }
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
	            	progressBar.setVisibility(View.INVISIBLE);
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0){
//							UserInfo userInfo= UserInfo.getInstance(context);	
							Utils.showAlertDialog(jobject.getString("error_message"), Activity_Party_Details_Read.this);
//							userInfo.saveUserInfo(context);
						}
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
	
	             }
	    	});


		}
		
		public void changeList(String event_id,String  high_five){
		    	for(int i=0;i<Activity_Party_Details.json_array_list.size();i++){
		    		JSONObject jsonObj = Activity_Party_Details.json_array_list.get(i);
		    		try {
						if(jsonObj.getString("event_id").equals(event_id)){
							jsonObj.remove("high_five");
							jsonObj.put("high_five", high_five);
//							if(update_deal_data==1)
							Activity_Party_Details.json_array_list.set(i, jsonObj);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
		    	
		}

}

package com.tbox.campus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Add_Event extends Activity_Base implements OnClickListener{
	EditText eventName,eventDate,eventAddress,eventWebsite,eventContactNumber,eventDescription,eventTitle1,eventTitle2;
	Button 	 eventAddBtn;
	ImageView imageView;
	ProgressBar pbAddEvent;
	private Uri outputFileUri,selectedImageUri;
	String picturePath;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Add Your Event");
		super.setBaseLayout(R.layout.activity_add_event);
		Registerwidgets();
		setListner();
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		eventDescription.setSingleLine(false);

	}
	private void Registerwidgets(){
		eventName=(EditText)findViewById(R.id.addEventName);
		eventDate=(EditText)findViewById(R.id.addEventDate);
		eventAddress=(EditText)findViewById(R.id.addEventAddress);
		eventWebsite=(EditText)findViewById(R.id.addEventWebsite);
		eventContactNumber=(EditText)findViewById(R.id.addEventContactNumber);
		eventDescription=(EditText)findViewById(R.id.addEventDescription);
		eventAddBtn=(Button)findViewById(R.id.addEventSaveChange);
		imageView=(ImageView)findViewById(R.id.imageviewAddEvent);
		pbAddEvent=(ProgressBar)findViewById(R.id.progressAddEvent);
		eventTitle1=(EditText)findViewById(R.id.addEventTitleOne);
		eventTitle2=(EditText)findViewById(R.id.addEventTitleTwo);
	}
	private void setListner(){
		eventAddBtn.setOnClickListener(this);
		imageView.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.addEventSaveChange:
				if(eventName.length()!=0&&eventAddress.length()!=0&&eventDate.length()!=00&&eventWebsite.length()!=0&&
					eventContactNumber.length()!=0&&eventDescription.length()!=0&&eventTitle1.length()!=0&&eventTitle2.length()!=0){
					if(picturePath==null)
						Utils.showAlertDialog("Please select image", Activity_Add_Event.this);
					else{
						pbAddEvent.setVisibility(View.VISIBLE);
						sendJsonRequestLoopi();
					}
				}else
					Utils.showAlertDialog("Your Field is Empty", Activity_Add_Event.this);
				break;
			case R.id.imageviewAddEvent:
				openImageIntent();
				break;
		}
	}
	
	private void openImageIntent() {

		// Determine Uri of camera image to save.
		final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
		root.mkdirs();
		final String fname = "AddEvent.jpg";
		final File sdImageMainDirectory = new File(root, fname);
		picturePath = sdImageMainDirectory.toString();
		outputFileUri = Uri.fromFile(sdImageMainDirectory);

		    // Camera.
		    final List<Intent> cameraIntents = new ArrayList<Intent>();
		    final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		    final PackageManager packageManager = getPackageManager();
		    final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
		    for(ResolveInfo res : listCam) {
		        final String packageName = res.activityInfo.packageName;
		        final Intent intent = new Intent(captureIntent);
		        intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
		        intent.setPackage(packageName);
		    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
		        cameraIntents.add(intent);
		    }

		    // Filesystem.
		    final Intent galleryIntent = new Intent();
		    galleryIntent.setType("image/*");
		    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

		    // Chooser of filesystem options.
		    final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

		    // Add the camera options.
		    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

		    startActivityForResult(chooserIntent, 1);
		}

		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data)
		{
			boolean flagStatus;
		    if(resultCode == RESULT_OK)
		    {
		        if(requestCode == 1)
		        {
		            final boolean isCamera;
		            if(data == null)
		            {
		                isCamera = true;
		            }
		            else
		            {
		                final String action = data.getAction();
		                if(action == null)
		                {
		                    isCamera = false;
//		                    flagStatus= true;
		                }
		                else
		                {
		                    isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		                }
		            }

//		            Uri selectedImageUri;
		            if(isCamera){
		                selectedImageUri = outputFileUri;
		                flagStatus=true;
//		                picturePath = selectedImageUri.toString();
		            }else{
		                selectedImageUri = data == null ? null : data.getData();
		                flagStatus=false;
		            }
		            try {
						imageView.setImageBitmap(Utils.decodeUri(selectedImageUri,Activity_Add_Event.this));
						if(flagStatus)
							saveBitmap(selectedImageUri , true);
						else
							saveBitmap(selectedImageUri , false);
//						if(flagStatus){
//							String[] filePathColumn = { MediaStore.Images.Media.DATA };
//							Cursor cursor = getContentResolver().query(selectedImageUri,filePathColumn, null, null, null);
//					        cursor.moveToFirst();
//					        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//					        picturePath = cursor.getString(columnIndex);
//					           cursor.close();
//
//						}else{
//							picturePath = selectedImageUri.toString().substring(7);
							
//						}
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		    }
		}
		private void sendJsonRequestLoopi(){
			// Putting data into Params
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
			RequestParams params = new RequestParams();
			JSONObject json = new JSONObject();
		    try {
				json.put("event_name",eventName.getText().toString());
				json.put("event_date",eventDate.getText().toString());
				json.put("phone_number",eventContactNumber.getText().toString());
				json.put("website",eventWebsite.getText().toString());
				json.put("address",eventAddress.getText().toString());
				json.put("special_instructions",eventDescription.getText().toString());
				json.put("event_deal1",eventTitle1.getText().toString());
				json.put("event_deal2",eventTitle2.getText().toString());
				json.put("api_key",settings.getString("key", null).toString());
				
			}catch (JSONException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			File myFile = new File(picturePath);
//		    try {
//				params.put("image", myFile);
//			} catch (FileNotFoundException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
		    params.put("data", json.toString());
	        //send get request for New Details
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.post(Constant.addess_url_base + Constant.address_add_event, params , new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
	            	int status = 0;
					try {
						status = object.getInt("status");
						 if(status ==1){
							 showAlertDialog(object.getString("success_message"));
		 				}
			 			pbAddEvent.setVisibility(View.INVISIBLE);
					
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
	            	
	             }
	             @Override
	             public void onSuccess(int statusCode, JSONObject response) {
	            	Log.d("testing", response.toString());
//	            	pbAddEvent.setVisibility(View.INVISIBLE);
	            	int status = 0;
						try {
							status = response.getInt("status");
							 if(status ==1){ 
								 	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Add_Event.this);
									RequestParams params = new RequestParams();
									File myFile = new File(picturePath);
								    try {
										params.put("image", myFile);
									} catch (FileNotFoundException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								    
							    	AsyncHttpClient client = new AsyncHttpClient();
							    	client.setTimeout(60000);
							    	client.post(Constant.addess_url_base + Constant.address_add_event_image+"/"+settings.getString("key", null).toString()+"/"+response.getString("event_id")
							    			, params , new JsonHttpResponseHandler(){  

							             @Override
							             public void onSuccess(final JSONObject object){
							            	int status = 0;
											try {
												status = object.getInt("status");
												 if(status ==1){

													 showAlertDialog(object.getString("success_message"));
								 				}
									 			pbAddEvent.setVisibility(View.INVISIBLE);
											
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											
							            	
							             }
							             @Override
							             public void onSuccess(int statusCode, JSONObject response) {
							            	Log.d("testing", response.toString());
							            	pbAddEvent.setVisibility(View.INVISIBLE);
							            	int status = 0;
												try {
													status = response.getInt("status");
													 if(status ==1){ 
														 showAlertDialog(response.getString("success_message"));
									 				}
												
												} catch (JSONException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							             }
							             @Override
							             public void onFailure(Throwable e, JSONObject errorResponse) {
						 	             Log.d("testing", errorResponse.toString());
							             }
							             @Override
							             protected void handleFailureMessage(Throwable e, String responseBody) {
							            	pbAddEvent.setVisibility(View.INVISIBLE);
							 				try{
												JSONObject jobject =  new JSONObject(responseBody);
												int status = jobject.getInt("status");
												if(status ==0)
													Utils.showAlertDialog(jobject.getString("error_message"), Activity_Add_Event.this);
												
											} catch (Exception ex) {
												// TODO Auto-generated catch block
												ex.printStackTrace();
											}

							             }
							    	});

//								 showAlertDialog(response.getString("success_message"));
			 				}
						
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	             }
	             @Override
	             public void onFailure(Throwable e, JSONObject errorResponse) {
 	             Log.d("testing", errorResponse.toString());
	             }
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
	            	pbAddEvent.setVisibility(View.INVISIBLE);
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0)
							Utils.showAlertDialog(jobject.getString("error_message"), Activity_Add_Event.this);
						
					} catch (Exception ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}

	             }
	    	});

		}
		private void showAlertDialog(String message)
		{
			new AlertDialog.Builder(Activity_Add_Event.this)
			.setMessage(message)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
					
						finish();
					}
				})
				.show();
		}
		private void saveBitmap(Uri uri,boolean imageFlag)
		{
				ContentResolver cr = getContentResolver();
				InputStream is = null;
				try {
					is = cr.openInputStream(uri);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Options optionSample = new BitmapFactory.Options();
				optionSample.inSampleSize = 4; // Or 8 for smaller image
				Bitmap bMap = BitmapFactory.decodeStream(is, null, optionSample);
//				bMap.getHeight();
//				bMap.getWidth();
//				File myDir = new File(imagePath);
//				myDir.mkdir();
				if(bMap.getHeight()>500&&bMap.getWidth()>500){
					File file = new File(picturePath);
					if(!file.exists())
						file.mkdir();
					Log.i("path", "" + file);
					try {
						FileOutputStream out = new FileOutputStream(file);
						bMap.compress(Bitmap.CompressFormat.JPEG, 100, out);
						out.flush();
						out.close();
					} catch (Exception e) {
							e.printStackTrace();
					} 
				}else if(!imageFlag){
					String[] filePathColumn = { MediaStore.Images.Media.DATA };
					Cursor cursor = getContentResolver().query(uri,filePathColumn, null, null, null);
			        cursor.moveToFirst();
			        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			        picturePath = cursor.getString(columnIndex);
			        cursor.close();
				}
//				}else{
//					
//				}
					
		}

}

package com.tbox.campus;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.classes.SearchData;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Search extends Activity implements OnClickListener{
	Button 		btnCancel;
	EditText	editText;
	ListView	listView;
	ArrayList<SearchData> searchListView;
	ArrayList<JSONObject>	json_array_list;
	ProgressBar pb;
	boolean    	flagCoupens=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
//		super.setHeader(false,false,"News");
//		super.setBaseLayout(R.layout.activity_search);
		setContentView(R.layout.activity_search);
		
		registerWidgets();
		setListner();
		listViewListener();
		
		editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.length()>2){
					ResultForSeacrh(s.toString());
					flagCoupens=false;
				}else if(s.length()<3){
//					searchListView = new ArrayList<SearchDataClass>();
					listView.setVisibility(View.INVISIBLE);
					flagCoupens=true;
				}
			}
		});
	}
	private void registerWidgets(){
		 btnCancel = (Button)findViewById(R.id.btnCancelSearch);
		 editText  = (EditText)findViewById(R.id.editTextSearch);
		 listView  = (ListView)findViewById(R.id.listSearch);
		 pb	= (ProgressBar)findViewById(R.id.progressBarSearch);
		 json_array_list =	new ArrayList<JSONObject>();
	}
   private void setListner(){
	   btnCancel.setOnClickListener(this);
  	}
   @Override
   public void onClick(View v) {
	// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnCancelSearch:
				InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//				getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				finish();
			break;
		default:
			break;
		}
	
   }
   private void listViewListener(){
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent 	=	new Intent(Activity_Search.this,Activity_Deal_Category_Details.class);
				Bundle bundle	=	new Bundle();
				bundle.putInt("status", 1);
				bundle.putInt("value", 0);
				bundle.putString("jsonObject", json_array_list.get(arg2).toString());
				intent.putExtras(bundle) ;
				startActivity(intent);	
			}
		});

		
	}
   private void ResultForSeacrh(String searchString){
	   	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Search.this);
	   	RequestParams params = new RequestParams();
	   
	   	params.put("search_string",searchString);
	   	params.put("api_key",settings.getString("key", null).toString());
	   
	
	 //send get request for New Details
   		AsyncHttpClient client = new AsyncHttpClient();
   		client.get(Constant.addess_url_base+Constant.address_search, params , new JsonHttpResponseHandler(){  

   		@Override
            public void onSuccess(final JSONObject object){
   			pb.setVisibility(View.VISIBLE);
           	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
						 	searchListView = new ArrayList<SearchData>();
							JSONArray jArray1=object.getJSONArray("coupons");
							for(int i=0;i<jArray1.length();i++){
								JSONObject jobject1 =jArray1.getJSONObject(0);
								SearchData searchDataObj = new SearchData();
//								searchDataObj.kCoupen_Id= jobject1.getString("coupon_id");
								searchDataObj.kPlace_At = jobject1.getString("place_at");
								searchDataObj.kCoupon_Title = jobject1.getString("coupon_title");
								searchDataObj.kCoupon_Image = jobject1.getString("coupon_image");
//								searchDataObj.kDescription = jobject1.getString("description");
//								searchDataObj.kCoupons_Company_Website = jobject1.getString("coupons_company_website");
//								searchDataObj.kCompany_Phone_Number = jobject1.getString("company_phone_number");
								searchDataObj.kHigh_Five = jobject1.getString("high_five");
								searchListView.add(searchDataObj);
								json_array_list.add(jobject1);  
							}
							pb.setVisibility(View.INVISIBLE);
							if(!flagCoupens){
								listView.setVisibility(View.VISIBLE);
								ListAdapter1 listAdapter =  new ListAdapter1();
								listView.setAdapter(listAdapter);
							}

					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
           	
            }
            @Override
            protected void handleFailureMessage(Throwable e, String responseBody) {
           	pb.setVisibility(View.INVISIBLE);
				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
//					if(status ==0)
//						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Search.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

            }
   	});

   }
	class ListAdapter1 extends ArrayAdapter<SearchData>
    {

 	public ListAdapter1() {
 		super(Activity_Search.this, R.layout.inflate_search_view,searchListView);
 		// TODO Auto-generated constructor stub
 	}
// 	@SuppressLint("SdCardPath")
	@Override
 		public View getView(final int position, View convertView, ViewGroup parent) {
 			// TODO Auto-generated method stub
				if(convertView==null){
					LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 		    convertView = inflater.inflate(R.layout.inflate_search_view, null);
				}
				ImageView tv_imagePath = (ImageView)convertView.findViewById(R.id.imageviewCoupen);
				TextView tv_title = (TextView)convertView.findViewById(R.id.coupentitle);
				TextView tv_place = (TextView)convertView.findViewById(R.id.coupenPlace);
				TextView tv_high_five = (TextView)convertView.findViewById(R.id.tvSearchHighFiveText);
				tv_title.setText(searchListView.get(position).kCoupon_Title);
				tv_place.setText(searchListView.get(position).kPlace_At);
				tv_high_five.setText(searchListView.get(position).kHigh_Five);
	 			ImageLoader imgLoader = new ImageLoader(Activity_Search.this);
	 			imgLoader.DisplayImage(searchListView.get(position).kCoupon_Image, tv_imagePath);

 			return convertView;
 		}
    }
	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		super.onBackPressed();
	}
	
	

}

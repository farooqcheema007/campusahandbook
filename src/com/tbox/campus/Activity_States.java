	package com.tbox.campus;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.classes.States;

@SuppressLint("NewApi")
public class Activity_States extends Fragment implements OnItemSelectedListener,OnClickListener {
	   RelativeLayout rl = null;
       String[]				states;
       String[]				schols = null;
       String[]				schoolForFirstIndexOfState; 
       int[]				campusid,statesid;
       JSONArray 			jArray1=null;
       Spinner 				my_spin_school,my_spin_state;
       ArrayAdapter<String> st;
       ArrayAdapter<String> sch;
       States[] 		object;
       ArrayList<States> info;
       SharedPreferences 	settings;
       JSONArray 			jArray;
       List<NameValuePair> 	userInfo;
       ProgressBar			pb_progressbar_states;
       boolean 				flag = false, flagSpinner = false;;
       int 					count = 0;
       boolean 				valueButton;
       
   private void registerWidgets(View v){
 		 my_spin_state	=	(Spinner)v.findViewById(R.id.states);
		 my_spin_school		=	(Spinner)v.findViewById(R.id.campus);
//		 btn_submit	=	(Button)findViewById(R.id.submit);
		 pb_progressbar_states = (ProgressBar)v.findViewById(R.id.progressBarStates);
		 rl = (RelativeLayout)v.findViewById(R.id.stateRelativeLayout);
	}
    private void setListner(){
//    	btn_submit.setOnClickListener(this);
		my_spin_state.setOnItemSelectedListener(this);
		my_spin_school.setOnItemSelectedListener(this);
   	}   
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		super.setHeader(false,false,"");
//		super.setBaseLayout(R.layout.activity_states);
//		//Bundle bindle=Intent.
//		registerWidgets();
//		sendJsonRequestLoopi();   
//		setListner();
//	}
	private void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_states, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject jobject){
            	int status = 0;
				try{
					status = jobject.getInt("status");
					if(status ==1){ 
						jArray1=jobject.getJSONArray("states");
						object=new States[jArray1.length()];
						states=new String[jArray1.length()+1];
						statesid=new int[jArray1.length()+1];
						states[0]="Select States";
						statesid[0]=0;
						for(int i=0;i<jArray1.length();i++){
							object[i]=new States();
							JSONObject jobject1 = jArray1.getJSONObject(i);
							jobject1.getString("state_id");
							states[i+1]=jobject1.getString("state_name");
							statesid[i+1]=Integer.parseInt(jobject1.getString("state_id"));
							object[i].setStatename(jobject1.getString("state_name"));
							object[i].setState(Integer.parseInt(jobject1.getString("state_id")));
							JSONArray  sch=jobject1.getJSONArray("schools");
							schols=new String[sch.length()+1];
							campusid=new int[sch.length()+1];
							schols[0]="Select Campus";
							campusid[0]=0;
							for(int j=0;j<sch.length();j++){
								JSONObject jobject2 =sch.getJSONObject(j);
								schols[j+1]=jobject2.getString("school_name");
								campusid[j+1]=Integer.parseInt(jobject2.getString("school_id"));
							}
							object[i].setSchooolname(schols);
							object[i].setSchooolid(campusid);
						} 
						pb_progressbar_states.setVisibility(View.INVISIBLE);
						
						
						st=new ArrayAdapter<String>(Activity_Main_Fragment.context, android.R.layout.simple_spinner_item,states);
						st.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						my_spin_state.setSelection(states.length-1);
						
						my_spin_state.setAdapter(st);
					}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_progressbar_states.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Main_Fragment.context);
					}
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}



	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
	
		Spinner spinner = (Spinner) arg0;
	    if(spinner.getId() == R.id.states){
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
			SharedPreferences.Editor editor = settings.edit();
			if (arg2==0){
	    		 	schoolForFirstIndexOfState = new String[1];
	    		 	schoolForFirstIndexOfState[0] = "Select Campus";
	    		 	sch=new ArrayAdapter<String>(Activity_Main_Fragment.context, android.R.layout.simple_spinner_item,schoolForFirstIndexOfState);
	      	        sch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	      	        my_spin_school.setAdapter(sch);
	      			my_spin_school.setEnabled(false);
	      			my_spin_school.setClickable(false);
	      			flag = true;

	    	 }else{
		         for(int i=0;i<object.length;i++){
		        	  String temp=object[i].getstatename();
		        	  if(states[arg2].equals(temp))
		        	  {
		        		info=new ArrayList<States>();
		        		info.add(object[i]);
		        		editor.putString("state",object[i].getstatename()).commit();
		        		
		        		sch=new ArrayAdapter<String>(Activity_Main_Fragment.context, android.R.layout.simple_spinner_item,object[i].getstateschool());
		      	        sch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		      	        sch.set
		      	        my_spin_school.setAdapter(sch);
		      			my_spin_school.setEnabled(true);
		      			my_spin_school.setClickable(true);
		      			flag = false;
//		      			count++;
		      	        break;
		      		}
		          }
	    	 }
	     }else if(spinner.getId() == R.id.campus){
	 		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_Main_Fragment.context);
			SharedPreferences.Editor editor = settings.edit();
			if(!flag){ 
		    	for(int j=0;j<info.size();j++){
		            String []names=info.get(j).getstateschool();
		    		int []sid=info.get(j).getschoolids();
		            	for(int k=0;k<names.length;k++)
					{
						if(arg0.getItemAtPosition(arg2).toString().equals(names[k]))
						{
							 editor.putInt("schoolid",sid[k]).commit();
							 editor.putString("schoolname",arg0.getItemAtPosition(arg2).toString()).commit();
							 if(!spinner.getSelectedItem().toString().equals("Select Campus"))
								 showAlertDialog("Record Saved..!");		
//							 else if()
//								 count++;
						}
					}
					
				}
			}
	     }
		
		
	     }
	

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
    private void showAlertDialog(String message)
	{
		new AlertDialog.Builder(Activity_Main_Fragment.context)
		.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (getActivity() instanceof Activity_Main_Fragment) {
					Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
        			ra.callDealActivity();
        		}
//				   Intent intent_deal = new Intent(Activity_States.this,Activity_Home.class);
////				   intent_deal.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				   startActivity(intent_deal);
//				   finish();

			}
		})
		.show();
	}
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	   super.onActivityResult(requestCode, resultCode, data);
//	   if (resultCode == RESULT_OK && requestCode == 1) {
//	      setResult(RESULT_OK);
//	      finish();
//	   }
//	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	   @Override
	   public View onCreateView(LayoutInflater inflater,
	      ViewGroup container, Bundle savedInstanceState) {
	      /**
	       * Inflate the layout for this fragment
	       */
	      View v =  inflater.inflate(R.layout.activity_states, container, false);
	      registerWidgets(v);
	      setListner();
	      if (getActivity() instanceof Activity_Main_Fragment) {
	    	  Activity_Main_Fragment ra = (Activity_Main_Fragment) getActivity();
  			  ra.setFooter(rl);
  			  if(!valueButton)
  				  ra.setHeader(false,false,"" ,rl);
  			  else
  				  ra.setHeader(true,false,"" ,rl);
  		  }
//	      registerWidgets(v);
//	      setListner();
	      sendJsonRequestLoopi();
//	      RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
//	      params.addRule(RelativeLayout.BELOW,MainActivity.rl.getId());
//	      v.setLayoutParams(params);
//	      Button btnSignup = (Button) v.findViewById(R.id.buttonFirstActivity); 
//	      btnSignup.setOnClickListener(new View.OnClickListener() {
//	            public void onClick(View v) {
//	            	if (getActivity() instanceof MainActivity) {
//	        			MainActivity ra = (MainActivity) getActivity();
//	        			ra.toogleFragment();
//	        		}
////	            	MainActivity mainActivity 
////	            	Toast.makeText(getActivity(), "yeah you click me", Toast.LENGTH_SHORT).show();	
//	            }

//	        });

	      return v;
	   }
	   


}

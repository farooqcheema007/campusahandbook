package com.tbox.campus;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

public class UserInfo {
	private static final int MODE_PRIVATE = 0;
	public String api_key;
	public String first_name;
	public String last_name;
	public String password;
	public String profile_image_path = "";
	public String email_id;
	public String gender;
	public int noti_push_woosh;
	public int noti_email;	 
	public String profile_spinner="";
	public Boolean is_login=false;
	private static UserInfo   _instance;

	    
    public static UserInfo getInstance(Context context)
    {
        if (_instance == null)
        {
            _instance = getUserInfo(context);
        }
        return _instance;
    }
    
    public void saveUserInfo(Context context){
    	this.is_login=true;
    	SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(this);
        prefsEditor.putString("UserInfo", json);
        prefsEditor.commit();
    }
    public void removeUserInfo(Context context){
    	this.is_login=false;
    	SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	this.api_key="";
        this.first_name="";
        this.last_name="";
        this.profile_image_path="";
        this.email_id="";
        this.gender="";
        this.password="";
        this.profile_spinner ="";
        this.noti_push_woosh=0;
        this.noti_email=0;
        Gson gson = new Gson();
        String json = gson.toJson(this);
    	Editor prefsEditor = prefs.edit();
        prefsEditor.putString("UserInfo", json);
        prefsEditor.commit();
    }
    
    private static UserInfo getUserInfo(Context context){
    	  Gson gson = new Gson();
    	  SharedPreferences prefs = context.getSharedPreferences("userInfo",MODE_PRIVATE);
    	  String json = prefs.getString("UserInfo", null);
    	  if(json != null){
    		  UserInfo userInfo = gson.fromJson(json, UserInfo.class);
    		  if(userInfo.api_key.length() >0){
    			  userInfo.is_login=true;
    		  }
    		  return userInfo;
    	  }else{
    		   return new UserInfo();
    	  }
    }
}

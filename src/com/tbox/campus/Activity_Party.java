package com.tbox.campus;

import java.util.ArrayList;
import com.tbox.classes.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Party extends Activity_Base implements OnClickListener{
	ArrayList<Party> 	eventList;
	ProgressBar				pb_events;
	GridView 				gridView;
	static int 				height,width;
//	Button 					btn_add_event;
	
	private void Registerwidgets(){
		eventList = new ArrayList<Party>();
		pb_events = (ProgressBar)findViewById(R.id.progressBarEvent);
		gridView = (GridView) findViewById(R.id.gridView1);
//		btn_add_event=(Button)findViewById(R.id.partyAddEvents);
	}
	private void setListner(){
//		btn_add_event.setOnClickListener(this);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,true,"Party");
		super.setBaseLayout(R.layout.activity_party);
//		height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, getResources().getDisplayMetrics());
		android.view.Display display = ((android.view.WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int screenWidth = (int)(display.getWidth());
        int screenHeight = (int)(display.getHeight());
        width = (int)(screenWidth*0.40);
        height = (int)(screenHeight*0.25);
		Registerwidgets();
		setListner();
		sendJsonRequest();
	}
	private void sendJsonRequest(){
		sendJsonRequestLoopi();
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
//				v.setBackgroundColor(0xFF8f8f8f);
				Intent intent_deal_services =new Intent(Activity_Party.this,Activity_Party_Details.class);
				Bundle bundle_services =new Bundle();
				bundle_services.putString("category_id",eventList.get(position).event_category_id);
				bundle_services.putString("event_category",eventList.get(position).event_category);
				intent_deal_services.putExtras(bundle_services) ;
				startActivity(intent_deal_services);
//				}
			}
		});
	}
	private void sendJsonRequestLoopi(){
		// Putting data into Params
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		String id=""+settings.getInt("schoolid", 0);
		params.put("school_id",id.toString());
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_party, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try{
					status = object.getInt("status");
					if(status ==1){ 
							JSONArray jArray1=object.getJSONArray("party_categories");
							for(int i=0;i<jArray1.length();i++){
								JSONObject	jobject1 = jArray1.getJSONObject(i);
								Party objPartyData = new Party();
								objPartyData.event_category=jobject1.getString("event_category");
								objPartyData.event_category_image=jobject1.getString("event_category_image");
								objPartyData.event_category_id=jobject1.getString("event_category_id");
					              
					            eventList.add(objPartyData);
					        }
							Party objPartyData = new Party();
							objPartyData.event_category="Button";
							objPartyData.event_category_image="Button";
							objPartyData.event_category_id="Button";
				              
				            eventList.add(objPartyData);

							pb_events.setVisibility(View.INVISIBLE);
							gridView.setAdapter(new BaseAdapterParty(eventList,Activity_Party.this));
					}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb_events.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
//						UserInfo userInfo= UserInfo.getInstance(context);	
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Party.this);
//						userInfo.saveUserInfo(context);
					}
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});

	}
//	@Override
//	public void onClick(View v) {
//		// TODO Auto-generated method stub
//		switch (v.getId()) {
//			case R.id.partyAddEvents:
//				Intent news_intent =new Intent(Activity_Party.this,Activity_Add_Event.class);
//				startActivity(news_intent);
//				break;
//		}
//	}

}

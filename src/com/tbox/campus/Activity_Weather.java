package com.tbox.campus;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campus.R;
import com.tbox.classes.Weather;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Weather extends Activity_Base {
	ListView							listview;
	ArrayList<Weather>					weatherList;
	ArrayList<HashMap<String, String>> 	weather;
	ArrayList<String>					wied;
	WebView 							webView;
	ProgressBar 						pb_weather;
	String 								temp,high,low,sheher,imgurl,cityname,code,region,centi;
	TextView 							tv_text,tv_city,tv_lowtemp,tv_hightemp;
	ImageView 							iv_image;
	final String yahooPlaceApisBase = 	"http://query.yahooapis.com/v1/public/yql?q=select*from%20geo.places%20where%20text=";
	final String 						yahooapisFormat = "&format=xml";
	String 								yahooPlaceAPIsQuery;
	public ImageLoader 					imageLoader; 
	Context								context;
	String 								weatherString;
	
	private void Registerwidgets(){
		context		=	this;
		imageLoader	=	new ImageLoader(getApplicationContext());
		tv_text		=	(TextView)findViewById(R.id.temp);
		tv_city		=	(TextView)findViewById(R.id.city);	  
		tv_lowtemp	=	(TextView)findViewById(R.id.low);
		tv_hightemp	=	(TextView)findViewById(R.id.high);
		iv_image	=	(ImageView)findViewById(R.id.weatherimage);
		pb_weather	=	(ProgressBar)findViewById(R.id.progressBar1);
		weatherList	=	new ArrayList<Weather>();
		listview	=	(ListView)findViewById(R.id.weatherList);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setHeader(false,false,"");
		super.setBaseLayout(R.layout.activity_weather);

//		setContentView(R.layout.activity_weather);
		Registerwidgets();
		
		Thread myThread = new Thread(new Runnable(){
             
	    	   @Override
	    	   public void run() {
	    	    weatherString = QueryYahooWeather();
	    	    if(weatherString.equals("hello")){
//	    	    	Toast.makeText(getApplicationContext(), "No Result..!", Toast.LENGTH_SHORT).show();
	    	    }else{
	    	           final Document weatherDoc = convertStringToDocument(weatherString);
	    	           parsedata(weatherDoc);
	    	    }
	    	          //final String weatherResult = parseWeatherDescription(weatherDoc);
	    	          runOnUiThread(new Runnable(){

	    	     @Override
	    	     public void run() {
	    	    	// webView.loadData(weatherResult, "text/html", "UTF-8");
	 	    	    if(weatherString.equals("hello")){
	 	    	    	pb_weather.setVisibility(View.INVISIBLE);
		    	    	Toast.makeText(getApplicationContext(), "No Result..!", Toast.LENGTH_SHORT).show();
		    	    }else{

	    	    	 setBackGroundImage();
	    	    	 tv_text.setText(temp + (char) 0x00B0 );
	    	    	 tv_text.bringToFront();
	    	    	 tv_city.setText(sheher+" , "+region);
	    	    	 tv_city.bringToFront();
	    	    	 tv_lowtemp.setText("L: "+low + (char) 0x00B0 );
	    	    	 tv_lowtemp.bringToFront();
	    	    	 tv_hightemp.setText("H: "+high + (char) 0x00B0 );
	    	    	 tv_hightemp.bringToFront();
//	    	    	 pb_weather.setVisibility(View.INVISIBLE);
  					ListAdapter1 listAdapter=new ListAdapter1();
  					listview.setAdapter(listAdapter);
		    	    }	    	     
	 	    	    }});
	    	          
	    	   }});
	      myThread.start();
	     
	     
	}
	void parsedata(Document srcDoc)
	{
		Node conditionNode = srcDoc.getElementsByTagName("yweather:condition").item(0);
		temp = conditionNode.getAttributes()
				.getNamedItem("temp")
				.getNodeValue()
				.toString();
		code = conditionNode.getAttributes()
				.getNamedItem("code")
				.getNodeValue()
				.toString();
		String	todayDate = conditionNode.getAttributes()
				.getNamedItem("date")
				.getNodeValue()
				.toString();
		String str[] = todayDate.split(",");
		String today = str[0];
		
		Node locationNode = srcDoc.getElementsByTagName("yweather:location").item(0);
    	sheher = locationNode.getAttributes()
				.getNamedItem("city")
				.getNodeValue()
				.toString();    
		region = locationNode.getAttributes()
				.getNamedItem("region")
				.getNodeValue()
				.toString();

//    	URL url;
//		try {
//			url = new URL("http://weather.yahooapis.com/forecastrss?w=2459115&u=f");
//			RssFeed feed = RssReader.read(url);
//			ArrayList<RssItem> rssItems = feed.getRssItems();
//	    	for(RssItem rssItem : rssItems) {
//	    		Log.i("RSS Reader", rssItem.getDescription());
//	    		imgurl= rssItem.getDescription().substring(11, 48);
//	    		
//	    	}
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SAXException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		weather = new ArrayList<HashMap<String, String>>();
		NodeList nodeListDescription = srcDoc.getElementsByTagName("yweather:forecast");
		if(nodeListDescription.getLength()>=0){
			for(int i=0; i<nodeListDescription.getLength(); i++){
				if(nodeListDescription.item(i).getAttributes().getNamedItem("day").getNodeValue().toString().equals(today)){
					low = nodeListDescription.item(i).getAttributes().getNamedItem("low").getNodeValue().toString();
					high = nodeListDescription.item(i).getAttributes().getNamedItem("high").getNodeValue().toString();
				}
				 Weather weatherObj  = new Weather();
				 weatherObj.day	=	nodeListDescription.item(i).getAttributes().getNamedItem("day").getNodeValue().toString();
				 weatherObj.Low	=	nodeListDescription.item(i).getAttributes().getNamedItem("low").getNodeValue().toString();
				 weatherObj.high	=	nodeListDescription.item(i).getAttributes().getNamedItem("high").getNodeValue().toString();
				 weatherObj.code	=	nodeListDescription.item(i).getAttributes().getNamedItem("code").getNodeValue().toString();
				 weatherList.add(weatherObj);
			}
		}else{
		}
		
		
	}
	private Document convertStringToDocument(String src){
		Document dest = null;
		
		DocumentBuilderFactory dbFactory =
				DocumentBuilderFactory.newInstance();
		DocumentBuilder parser;

		try {
			parser = dbFactory.newDocumentBuilder();
			dest = parser.parse(new ByteArrayInputStream(src.getBytes()));
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
			Toast.makeText(Activity_Weather.this, 
					e1.toString(), Toast.LENGTH_LONG).show();
		} catch (SAXException e) {
			e.printStackTrace();
			Toast.makeText(Activity_Weather.this, 
					e.toString(), Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(Activity_Weather.this, 
					e.toString(), Toast.LENGTH_LONG).show();
		}
		
		return dest;
	}

	private String QueryYahooWeather(){
		String qResult = "";
		wied=QueryYahooPlaceAPIs();
		if(wied.size()==0){
			return "hello";
		}else{
		String a=wied.get(0).toString();
		String queryString = "http://weather.yahooapis.com/forecastrss?w="+a+"&u=f";
		
		HttpClient httpClient = new DefaultHttpClient();
	    HttpGet httpGet = new HttpGet(queryString);
	    
	    try {
	    	HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
	    	
	    	if (httpEntity != null){
	    		InputStream inputStream = httpEntity.getContent();
	    		Reader in = new InputStreamReader(inputStream);
	    		BufferedReader bufferedreader = new BufferedReader(in);
	    		StringBuilder stringBuilder = new StringBuilder();
	    		
	    		String stringReadLine = null;

	    		while ((stringReadLine = bufferedreader.readLine()) != null) {
	    			stringBuilder.append(stringReadLine + "\n");	
	    		}
	    		
	    		qResult = stringBuilder.toString();	
	    	}
	    	return qResult;

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			//Toast.makeText(AndroidYahooWeatherDOMActivity.this, 
					//e.toString(), Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			//Toast.makeText(AndroidYahooWeatherDOMActivity.this, 
					//e.toString(), Toast.LENGTH_LONG).show();
		}
		}
	    return qResult;
	}

	 private ArrayList<String> QueryYahooPlaceAPIs(){
		 SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);	
		 String uriPlace = Uri.encode(settings.getString("state", null));
	    	
	    	yahooPlaceAPIsQuery = yahooPlaceApisBase
	    			+ "%22" + uriPlace + "%22"
	    			+ yahooapisFormat;
	    	
	    	String woeidString = QueryYahooWeather(yahooPlaceAPIsQuery);
	    	Document woeidDoc = convertStringToDocument1(woeidString);
	    	return  parseWOEID(woeidDoc);
	    }
	 private String QueryYahooWeather(String queryString){
	    	String qResult = "";
	    	
	    	HttpClient httpClient = new DefaultHttpClient();
	    	HttpGet httpGet = new HttpGet(queryString);
	    	
	    	try {
	    		HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
	    		
	    		if (httpEntity != null){
	    			InputStream inputStream = httpEntity.getContent();
	    			Reader in = new InputStreamReader(inputStream);
	    			BufferedReader bufferedreader = new BufferedReader(in);
	    			StringBuilder stringBuilder = new StringBuilder();
	    			
	    			String stringReadLine = null;
	    			
	    			while ((stringReadLine = bufferedreader.readLine()) != null) {
	    				stringBuilder.append(stringReadLine + "\n");	
	    			}
	    			
	    			qResult = stringBuilder.toString();	
	    		}	
	    	} catch (ClientProtocolException e) {
	    		e.printStackTrace();;	
	    	} catch (IOException e) {
	    		e.printStackTrace();	
	    	}
	    	
	    	return qResult;	
	    }
	 private ArrayList<String> parseWOEID(Document srcDoc){
	    	ArrayList<String> listWOEID = new ArrayList<String>();
	    	
	    	NodeList nodeListDescription = srcDoc.getElementsByTagName("woeid");
	    	if(nodeListDescription.getLength()>=0){
	    		for(int i=0; i<nodeListDescription.getLength(); i++){
	    			listWOEID.add(nodeListDescription.item(i).getTextContent());	
	    		}	
	    	}else{
	    		listWOEID.clear();	
	    	}
	    	
	    	return listWOEID;
	    }
	 private Document convertStringToDocument1(String src){
	    	Document dest = null;
	    	
	    	DocumentBuilderFactory dbFactory =
	    			DocumentBuilderFactory.newInstance();
	    	DocumentBuilder parser;
	    	
	    	try {
	    		parser = dbFactory.newDocumentBuilder();
	    		dest = parser.parse(new ByteArrayInputStream(src.getBytes()));	
	    	} catch (ParserConfigurationException e1) {
	    		e1.printStackTrace();	
	    	} catch (SAXException e) {
	    		e.printStackTrace();	
	    	} catch (IOException e) {
	    		e.printStackTrace();	
	    	}
	    	
	    	return dest;
	    }
	 
	 
		class ListAdapter1 extends ArrayAdapter<Weather>{

		 	public ListAdapter1() {
		 		super(Activity_Weather.this, R.layout.inflate_weather_list_item,weatherList);
		 		// TODO Auto-generated constructor stub
		 	}

			/* (non-Javadoc)
			 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
			 */
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				if(convertView==null){
					LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = inflater.inflate(R.layout.inflate_weather_list_item, null);
	 		    }
	 			TextView dayName = (TextView)convertView.findViewById(R.id.daytext);
	 			TextView dayHigh = (TextView)convertView.findViewById(R.id.hightext);
	 			TextView dayLow = (TextView)convertView.findViewById(R.id.lowtext);
	 			ImageView iv 	= (ImageView)convertView.findViewById(R.id.weatherImage);
	 			setInflateImage(weatherList.get(position).code,iv);
	 			dayName.setText(weatherList.get(position).day);
	 			dayHigh.setText(weatherList.get(position).high+ (char) 0x00B0 );
	 			dayLow.setText(weatherList.get(position).Low+ (char) 0x00B0 );
	 			if(position==weatherList.size()-1)
	 				pb_weather.setVisibility(View.INVISIBLE);
	 			return convertView;	
			}
		 	

		}
		void setBackGroundImage(){
			int codeWeather = Integer.parseInt(code);
			if (codeWeather == 6 || codeWeather == 10 || codeWeather == 11 || codeWeather == 12 ||
					codeWeather == 17 || codeWeather == 18 || codeWeather == 35)//rains
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_rain));
			else if(codeWeather == 13 || codeWeather == 14 || codeWeather == 15 || codeWeather == 16 ||	
							codeWeather == 41 || codeWeather == 42 || codeWeather == 43 || codeWeather == 46)//snowy)	
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_snowy));
			else if (codeWeather == 8 || codeWeather == 9 || codeWeather == 40)//Showers
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_showers));
			else if (codeWeather == 25 || codeWeather == 32 || codeWeather == 34 || codeWeather == 36)//sunny
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_sunny));
			else if (codeWeather == 30 || codeWeather == 44)//Cloudy Day
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_cloudy_day));
			else if (codeWeather == 20 || codeWeather == 21 || codeWeather == 22)//Hazy
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_hazy));
			else if (codeWeather == 29 || codeWeather == 27)//Cloudy Night
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_cloudy_night));
			else if (codeWeather == 23 || codeWeather == 24)//Windy
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_windy));
			else if (codeWeather == 26 || codeWeather == 28)//Cloudy
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_cloudy));
			else if (codeWeather == 31 || codeWeather == 33)//Night
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_night));
			else if (codeWeather == 1 || codeWeather == 2 || codeWeather == 3|| codeWeather == 4
					|| codeWeather == 37 || codeWeather == 38 || codeWeather == 39 
					|| codeWeather == 45 || codeWeather == 47)//T-Storm
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_t_storm));
			else if (codeWeather == 0)//Tornado
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_tornado));
			else if (codeWeather == 5 || codeWeather == 7)//Snow and Rain
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_snow_rain));
			else if (codeWeather == 19)//Dusty
				iv_image.setImageDrawable(getResources().getDrawable(R.drawable.w_dusty));
		}
		void setInflateImage(String strCode , ImageView iv){
			int codeWeather = Integer.parseInt(strCode);
			 	if (codeWeather == 0) 
		            iv.setImageDrawable(getResources().getDrawable(R.drawable.w_0));
			 	else if (codeWeather == 1)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_1));
		        else if (codeWeather == 2)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_2));
		        else if (codeWeather == 3)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_3));
		        else if (codeWeather == 4)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_4));
		        else if (codeWeather == 5)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_5));
		        else if (codeWeather == 6)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_6));
		        else if (codeWeather == 7)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_7));
		        else if (codeWeather == 8) 
		            iv.setImageDrawable(getResources().getDrawable(R.drawable.w_8));
			 	else if (codeWeather == 9)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_9));
		        else if (codeWeather == 10)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_10));
		        else if (codeWeather == 11)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_11));
		        else if (codeWeather == 12)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_12));
		        else if (codeWeather == 13)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_13));
		        else if (codeWeather == 14)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_14));
		        else if (codeWeather == 15)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_15));
		        else if (codeWeather == 16)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_16));
		        else if (codeWeather == 17)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_17));
		        else if (codeWeather == 18)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_18));
		        else if (codeWeather == 19)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_19));
		        else if (codeWeather == 20)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_20));
		        else if (codeWeather == 21)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_21));
		        else if (codeWeather == 22)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_22));
		        else if (codeWeather == 23)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_23));
		        else if (codeWeather == 24)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_24));
		        else if (codeWeather == 25)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_25));
		        else if (codeWeather == 26)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_26));
		        else if (codeWeather == 27)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_27));
		        else if (codeWeather == 28)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_28));
		        else if (codeWeather == 29)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_29));
		        else if (codeWeather == 30)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_30));
		        else if (codeWeather == 31)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_31));
		        else if (codeWeather == 32)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_32));
		        else if (codeWeather == 33)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_33));
		        else if (codeWeather == 34)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_34));
		        else if (codeWeather == 35)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_35));
		        else if (codeWeather == 36)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_36));
		        else if (codeWeather == 37)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_37));
		        else if (codeWeather == 38)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_38));
		        else if (codeWeather == 39)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_39));
		        else if (codeWeather == 40)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_40));
		        else if (codeWeather == 41)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_41));
		        else if (codeWeather == 42)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_42));
		        else if (codeWeather == 43)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_43));
		        else if (codeWeather == 44)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_44));
		        else if (codeWeather == 45)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_45));
		        else if (codeWeather == 46)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_46));
		        else if (codeWeather == 47)
		        	iv.setImageDrawable(getResources().getDrawable(R.drawable.w_47));

		        }
	}

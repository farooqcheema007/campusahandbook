package com.tbox.campus;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.campus.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.imagedownload.ImageLoader;

public class Activity_Greek_Details extends Activity_Base implements OnClickListener{
	TextView	tv_address,tv_phone_no,tv_website,iv_greek_details_subheading,tv_des,tv_high_five;
	ImageView	iv_greek,phone_icon,iv_main,website_btn,have_five_btn,navi_btn,call_btn;
	String 		strPhone,greek_company_website,greek_company_phone_No,group_id;
	ProgressBar	progressBar;
	
	private void Registerwidgets(){
  	  	tv_address=(TextView)findViewById(R.id.address);
  	  	tv_phone_no=(TextView)findViewById(R.id.phoneNumber);
  	  	tv_website=(TextView)findViewById(R.id.webSite);
  	  	iv_greek=(ImageView)findViewById(R.id.imageGreek);
  	  	iv_greek_details_subheading=(TextView)findViewById(R.id.tvGreekDetailSubHeading);
  	  	phone_icon=(ImageView)findViewById(R.id.phoneIcon);
  	  	tv_des=(TextView)findViewById(R.id.greekDetailDescription);
  	  	tv_high_five = (TextView)findViewById(R.id.tvGreekHighFiveDealImage);
		website_btn=(ImageView)findViewById(R.id.webSiteGreekDetail);
		call_btn=(ImageView)findViewById(R.id.callGreekDetail);
		navi_btn=(ImageView)findViewById(R.id.naviGreekDetail);
		have_five_btn=(ImageView)findViewById(R.id.highFiveGreekDetail);
		progressBar=(ProgressBar)findViewById(R.id.progressBarGreekLifeDetail);
  	  	
  	  	phone_icon.setOnClickListener(this);
  	  	website_btn.setOnClickListener(this);
  	  	call_btn.setOnClickListener(this);
  	  	navi_btn.setOnClickListener(this);
  	  	have_five_btn.setOnClickListener(this);
    }
	
	private void mainMenuSetting(JSONObject jsonOject){
		ImageLoader iv_loader_obj	=	new ImageLoader(Activity_Greek_Details.this);
		try {
			iv_loader_obj.DisplayImage(jsonOject.getString("group_image"), iv_greek);
			tv_address.setText(jsonOject.getString("group_name"));
			tv_phone_no.setText(jsonOject.getString("phone_number"));
			strPhone =	jsonOject.getString("phone_number");
			tv_website.setText(jsonOject.getString("website_address"));
			tv_des.setText(jsonOject.getString("description"));
			
			tv_website.setText(Html.fromHtml(
		            " <font color='blue'><u><a href=\""+"http://"+jsonOject.getString("website_address")+"\">"+jsonOject.getString("website_address") +"</a></u></font>"));
			tv_website.setMovementMethod(LinkMovementMethod.getInstance());
			 
			iv_greek_details_subheading.setText(jsonOject.getString("group_name"));
			tv_high_five.setText(jsonOject.getString("high_five"));
			greek_company_website = "http://"+jsonOject.getString("website_address");
			greek_company_phone_No = "tel:" +jsonOject.getString("phone_number");
			group_id = jsonOject.getString("group_id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(getIntent().getStringExtra("nameType").equals("Soro"))
			super.setHeader(false,true,"Sororities");
		else if(getIntent().getStringExtra("nameType").equals("Fra"))
			super.setHeader(false,true,"Fraternities");

		super.setBaseLayout(R.layout.activity_greek_details);
		JSONObject 	jsonObj = null;
		try {
			jsonObj = new JSONObject(getIntent().getStringExtra("jsonObject"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Registerwidgets();
		tv_des.setMovementMethod(new ScrollingMovementMethod());
		if(jsonObj!=null)
			mainMenuSetting(jsonObj);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.phoneIcon:
			 String uri = "tel:" +strPhone;
			 Intent intent_call = new Intent(Intent.ACTION_CALL);
			 intent_call.setData(Uri.parse(uri));
			 startActivity(intent_call);
			 break;
		case R.id.webSiteGreekDetail:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(greek_company_website));
		    startActivity(browserIntent);
		    break;
		case R.id.callGreekDetail:
			Intent intent_call_greek = new Intent(Intent.ACTION_CALL);
			intent_call_greek.setData(Uri.parse(greek_company_phone_No));
			startActivity(intent_call_greek);
			break;
		case R.id.highFiveGreekDetail:
			progressBar.setVisibility(View.VISIBLE);
			highFiveHitsStatus();
//			high_five_message.setVisibility(View.INVISIBLE);
			break;

		}
	}
	void highFiveHitsStatus(){
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
//		String id=""+settings.getInt("schoolid", 0);
		params.put("group_id",group_id);
		params.put("api_key",settings.getString("key", null).toString());
        
        //send get request for New Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.addess_url_base+Constant.address_greek_high_five, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try{
					status = object.getInt("status");
					if(status ==1){ 
						progressBar.setVisibility(View.INVISIBLE);
//						String strValue = object.getInt("high_five");
						tv_high_five.setText(object.getString("high_five"));
						if(getIntent().getStringExtra("nameType").equals("Soro"))
							changeListSorority(group_id,object.getString("high_five"));
						else if(getIntent().getStringExtra("nameType").equals("Fra"))
							changeListFraternity(group_id,object.getString("high_five"));
//						high_five_message.setText(object.getString("success_message"));
					}
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	progressBar.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
//						UserInfo userInfo= UserInfo.getInstance(context);	
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Greek_Details.this);
//						userInfo.saveUserInfo(context);
					}
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
    	});


	}
    public void changeListFraternity(String group_id,String  high_five){
    	for(int i=0;i<Activity_Fraternity.json_array_list.size();i++){
    		JSONObject 	jsonObj = Activity_Fraternity.json_array_list.get(i);
    		try {
				if(jsonObj.getString("group_id").equals(group_id)){
					jsonObj.remove("high_five");
					jsonObj.put("high_five", high_five);
					Activity_Fraternity.json_array_list.set(i, jsonObj);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    public void changeListSorority(String group_id,String  high_five){
    	for(int i=0;i<Activity_Sorority.json_array_list.size();i++){
    		JSONObject 	jsonObj = Activity_Sorority.json_array_list.get(i);
    		try {
				if(jsonObj.getString("group_id").equals(group_id)){
					jsonObj.remove("high_five");
					jsonObj.put("high_five", high_five);
					Activity_Sorority.json_array_list.set(i, jsonObj);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }



}

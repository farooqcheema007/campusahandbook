package com.tbox.classes;

public class NewsDetail {
	public String category_id;
	public String news_image;
	public String news_id;
	public String date;
	public String school_id;
	public String news_title;
	public String news_description;
}
